<?php

use Illuminate\Database\Seeder;
use FE_UNSIQ\Eloquent\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// first truncate the table
    	Role::truncate();

        Role::create(['name' => 'Administrator']);
        Role::create(['name' => 'Mahasiswa']);
    }
}
