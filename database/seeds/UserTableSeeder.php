<?php

use FE_UNSIQ\Eloquent\MahasiswaProfil;
use Illuminate\Database\Seeder;
use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\User;
use FE_UNSIQ\Eloquent\Administrator;
use FE_UNSIQ\Eloquent\Role;

class UserTableSeeder extends Seeder
{
	public function run() 
	{
		// first trucate the table 
		User::truncate();
		DB::table('cms_user_role')->truncate();
		
		// create roles
		$this->createRoles();

		// create admin users
		foreach ($this->createAdministrator() as $admin) {
			$userAdmin = User::create([
				'username' => 'admin',
				'password' => 'admin',
				'owner_id' => $admin->id,
				'owner_type' => Administrator::class,
			]);
			$userAdmin->roles()->attach(1);
		}

		// create user mahasiswa
		foreach ($this->createMahasiswa() as $mahasiswa) {
			$userMahasiswa = User::create([
				'username' => trim($mahasiswa->nipd),
				'email' => trim($mahasiswa->email) == '' ? NULL : trim($mahasiswa->email),
				'password' => trim($mahasiswa->nipd),
				'owner_id' => $mahasiswa->id_reg_pd,
				'owner_type' => Mahasiswa::class,
			]);
			$userMahasiswa->roles()->attach([2]);
		}
	}

	protected function createRoles()
	{
		DB::table('cms_roles')->truncate();
		Role::create(['name' => 'Administrator']);
		Role::create(['name' => 'Mahasiswa']);
	}

	protected function createAdministrator()
	{
		DB::table('cms_administrator')->truncate();
		$admin = [];
		$admin[] = Administrator::create(['name' => 'Administrator']);

		return $admin;
	}

	protected function createMahasiswa()
	{
		$mahasiswa = Mahasiswa::join('mahasiswa', 'mahasiswa_pt.id_pd', '=', 'mahasiswa.id_pd')->groupBy('nipd')->get();

		return $mahasiswa;
	}
}