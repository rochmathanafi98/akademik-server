<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c11 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 55.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c18 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 282pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c41 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 350pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c45 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 72.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c21 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 168pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c37 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 267pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c14 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 74pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c28 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 222.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c38 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 222.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c46 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 420.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c33 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 276.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c29 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 74.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c20 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 72.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c0 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 74.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c4 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 34.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c35 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 99.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c9 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 93pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c7 {
            margin-left: -0.6pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c3 {
            margin-left: 30pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c2 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c19 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c44 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 14pt;
            font-family: "Arial";
            font-style: normal
        }

        .c47 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c31 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Arial";
            font-style: normal
        }

        .c5 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 9pt;
            font-family: "Arial";
            font-style: normal
        }

        .c10 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c26 {
            color: #000000;
            text-decoration: underline;
            vertical-align: baseline;
            font-family: "Arial";
            font-style: normal
        }

        .c6 {
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center;
            height: 11pt
        }

        .c36 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c42 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: left
        }

        .c25 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c22 {
            line-height: 1.0;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c8 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }

        .c13 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: center
        }

        .c1 {
            margin-left: auto;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c15 {
            margin-left: 0.9pt;
            orphans: 2;
            widows: 2
        }

        .c12 {
            font-size: 12pt;
            font-weight: 700;
            text-decoration: underline
        }

        .c17 {
            margin-left: 0.2pt;
            orphans: 2;
            widows: 2
        }

        .c30 {
            background-color: #ffffff;
            max-width: 555.3pt;
            padding: 28.3pt 28.3pt 28.3pt 28.3pt
        }

        .c24 {
            margin-left: 0.9pt;
            padding-top: 0pt;
            padding-bottom: 0pt
        }

        .c27 {
            orphans: 2;
            widows: 2
        }

        .c23 {
            font-size: 10pt;
            font-weight: 700
        }

        .c39 {
            line-height: 1.0;
            text-align: left
        }

        .c43 {
            line-height: 1.0
        }

        .c40 {
            font-size: 10pt
        }

        .c16 {
            height: 0pt
        }

        .c32 {
            height: 21pt
        }

        .c34 {
            height: 11pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c30">
<table class="c36">
    <tbody>
    <tr class="c32">
        <td class="c35" colspan="1" rowspan="1">
            <p class="c8"><span
                        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 97.00px; height: 92.33px;"><img
                            alt="LogoKRS.png" src="{{ URL::asset('assets/imgs/LogoKRS.png') }}"
                            style="width: 97.00px; height: 92.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                            title=""></span></p>
        </td>
        <td class="c46" colspan="1" rowspan="1">
            <p class="c25 c27"><span class="c31">PROGRAM PASCA SARJANA</span></p>
            <p class="c25 c27"><span class="c5">MAGISTER PENDIDIKAN ISLAM (M.Pd.I)</span></p>
            <p class="c25 c27"><span class="c5">MAGISTER STUDI ISLAM (M.S.I)</span></p>
            <p class="c25 c27"><span class="c44">UNIVERSITAS SAINS AL-QUR’AN</span></p>
            <p class="c25 c27"><span class="c47">JAWA TENGAH DI WONOSOBO</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c6"><span class="c40"></span></p>
<p class="c22"><span class="c23">DAFTAR MATERI AJAR SEMESTER</span></p>
<p class="c22"><span class="c23">PROGRAM PASCA SARJANA UNIVERSITAS SAINS AL-QUR'AN JAWA TENGAH DI WONOSOBO</span></p>
<p class="c22"><span class="c23">TAHUN AKADEMIK 2016/2017</span></p>
<p class="c27 c43 c34"><span class="c12"></span></p>
<table class="c1">
    <tbody>
    <tr class="c16">
        <td class="c45" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">Dosen</span></p>
        </td>
        <td class="c41" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">:
                    @if(! is_null($dosen))
                        {{ $dosen->nm_ptk }}
                    @endif
                        </span></p>
        </td>
        <td class="c29" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">Program Studi</span></p>
        </td>
        <td class="c38" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">: {{ $kelas->nm_lemb }}</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c20" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">Mata Kuliah</span></p>
        </td>
        <td class="c21" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">: {{ $kelas->nm_mk }} ({{ $kelas->sks_mk }} SKS)</span></p>
        </td>
        <td class="c0" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">Semester</span></p>
        </td>
        <td class="c28" colspan="1" rowspan="1">
            <p class="c25"><span class="c10">: {{ $kelas->smt }}</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c27 c34 c39"><span class="c23"></span></p>
<table class="c1">
    <tbody>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">PER</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c8 c17"><span class="c2">POKOK BAHASAN</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c7"><span class="c2">TANGGAL</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c8 c15"><span class="c2">JM. MHS</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">TANDA TANGAN</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8 c15"><span class="c2">I</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8 c15"><span class="c2">II</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">III</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">IV</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">V</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8 c15"><span class="c2">VI</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">VII</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">VIII</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">IX</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">X</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">XI</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">XII</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">XIII</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    <tr class="c16">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c22 c24"><span class="c2">XIV</span></p>
        </td>
        <td class="c18" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c3"><span class="c2">&nbsp;</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c6"><span class="c23"></span></p>
<p class="c6"><span class="c23"></span></p>
<table class="c1">
    <tbody>
    <tr class="c16">
        <td class="c37" colspan="1" rowspan="1">
            <p class="c42"><span class="c2">NB:</span></p>
            <p class="c42"><span class="c2">Mohon diisi setiap kali pertemuan</span></p>
        </td>
        <td class="c33" colspan="1" rowspan="1">
            <p class="c13"><span class="c2">Wonosobo, 12 Agustus 2016</span></p>
            <p class="c13"><span class="c2">PENGAMPU,</span></p>
            <p class="c13 c34"><span class="c2"></span></p>
            <p class="c13 c34"><span class="c2"></span></p>
            <p class="c13 c34"><span class="c2"></span></p>
            <p class="c13 c34"><span class="c2"></span></p>
            <p class="c13"><span class="c23 c26">
                    @if(! is_null($dosen))
                        {{ $dosen->nm_ptk }}
                    @endif
                </span></p>
        </td>
    </tr>
    </tbody>
</table>
</body>

</html>