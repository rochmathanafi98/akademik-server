<html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c29 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 2.2pt;
            width: 420.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c45 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 126pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c34 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 215.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c27 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 27.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c9 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 126pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c24 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 184.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c8 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 239.3pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c31 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 215.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c39 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 317.3pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c17 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 50.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c36 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 57pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c21 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 126pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c19 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 57pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c35 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 70pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c33 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 266.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c44 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 140.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c3 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 140.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c43 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 272.3pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c28 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 36.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c38 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 0pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 2.2pt;
            width: 99.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c20 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 140.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c30 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 0pt;
            border-left-color: #000000;
            vertical-align: middle;
            border-right-color: #000000;
            border-left-width: 0pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 0pt;
            width: 215.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c4 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c1 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c10 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c16 {
            color: #000000;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Arial";
            font-style: normal
        }

        .c5 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left;
        }

        .c37 {
            color: #000000;
            vertical-align: baseline;
            font-size: 10pt;
            font-family: "Arial";
            font-style: normal
        }

        .c25 {
            color: #000000;
            text-decoration: none;
            vertical-align: baseline;
            font-family: "Arial";
            font-style: normal
        }

        .c6 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }

        .c12 {
            margin-left: auto;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c7 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c41 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c13 {
            font-size: 12pt;
            font-weight: 700;
            text-decoration: underline
        }

        .c0 {
            background-color: #ffffff;
            max-width: 538.6pt;
            padding: 10pt 28.3pt 28.3pt 28.3pt
        }

        .c26 {
            line-height: 1.0;
            text-align: center
        }

        .c42 {
            font-weight: 400;
            font-size: 11pt
        }

        .c11 {
            orphans: 2;
            widows: 2
        }

        .c2 {
            font-size: 9pt;
            font-weight: 700
        }

        .c40 {
            text-align: left
        }

        .c15 {
            font-weight: 700
        }

        .c23 {
            text-decoration: underline
        }

        .c32 {
            line-height: 1.0
        }

        .c22 {
            height: 11pt
        }

        .c18 {
            height: 21pt
        }

        .c14 {
            height: 0pt
        }

        .c46 {
            font-size: 14pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c0">
<table class="c41">
    <tbody>
    <tr class="c18">
        <td class="c38" colspan="1" rowspan="1">
            <p class="c6"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 97.00px; height: 92.33px;"><img alt="LogoKRS.png" src="{{ URL::asset('assets/imgs/LogoKRS.png') }}" style="width: 97.00px; height: 92.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
        </td>
        <td class="c29" colspan="1" rowspan="1">
            <p class="c7 c11"><span class="c16 c15">PROGRAM PASCA SARJANA</span></p>
            <p class="c7 c11"><span class="c2 c25">MAGISTER PENDIDIKAN ISLAM (M.Pd.I)</span></p>
            <p class="c7 c11"><span class="c2">MAGISTER STUDI ISLAM (M.S.I)</span></p>
            <p class="c7 c11"><span class="c25 c15 c46">UNIVERSITAS SAINS AL-QUR’AN</span></p>
            <p class="c7 c11"><span class="c4">JAWA TENGAH DI WONOSOBO</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c11 c22 c32"><span></span></p>
<p class="c26 c11"><span class="c15">KARTU RENCANA STUDI (KRS)</span></p>
<p class="c26 c11"><span class="c15 c23">TAHUN AKADEMIK 2016/2017</span></p>
<p class="c11 c32 c22"><span class="c13"></span></p>
<table class="c12">
    <tbody>
    <tr class="c14">
        <td class="c21" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">Nama Mahasiswa</span></p>
        </td>
        <td class="c30" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">: {{ $mhs->nm_pd }}</span></p>
        </td>
        <td class="c35" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">Semester Mhs</span></p>
        </td>
        <td class="c20" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">: {{ $mhs->smt_mhs }}</span></p>
        </td>
    </tr>
    <tr class="c14">
        <td class="c45" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">Nomor Induk Mahasiswa</span></p>
        </td>
        <td class="c34" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">: {{ $mhs->nipd }}</span></p>
        </td>
        <td class="c36" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">Tahun Masuk</span></p>
        </td>
        <td class="c3" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">: {{ $mhs->mulai_smt }}</span></p>
        </td>
    </tr>
    <tr class="c14">
        <td class="c9" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">Program Studi</span></p>
        </td>
        <td class="c31" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">: {{ $mhs->nm_lemb }}</span></p>
        </td>
        <td class="c19" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">Jenjang</span></p>
        </td>
        <td class="c44" colspan="1" rowspan="1">
            <p class="c7"><span class="c10">: S2 (Magister)</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c11 c32 c22 c40"><span class="c15"></span></p>
<table class="c12">
    <tbody>
    <tr class="c14">
        <td class="c27" colspan="1" rowspan="1">
            <p class="c6"><span class="c1">NO</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6"><span class="c1">KODE</span></p>
        </td>
        <td class="c8" colspan="1" rowspan="1">
            <p class="c6"><span class="c1">MATAKULIAH</span></p>
        </td>
        <td class="c28" colspan="1" rowspan="1">
            <p class="c6"><span class="c1">SKS</span></p>
        </td>
        <td class="c24" colspan="1" rowspan="1">
            <p class="c6"><span class="c1">DOSEN</span></p>
        </td>
    </tr>
    {{--*/ $total_sks = 0 /*--}}
    @foreach ($krs as $i => $krs_mahasiswa)
    <tr class="c14">
        <td class="c27" colspan="1" rowspan="1">
            <p class="c6 c22"><span class="c10">{{ $i+1 }}</span></p>
        </td>
        <td class="c17" colspan="1" rowspan="1">
            <p class="c6 c22"><span class="c10">{{ $krs_mahasiswa->kode_mk }}</span></p>
        </td>
        <td class="c8" colspan="1" rowspan="1">
            <p class="c5"><span class="c10">{{ $krs_mahasiswa->nm_mk }}</span></p>
        </td>
        <td class="c28" colspan="1" rowspan="1">
            <p class="c6 c22"><span class="c10">{{ $krs_mahasiswa->sks_mk }}</span></p>
        </td>
        <td class="c24" colspan="1" rowspan="1">
            <p class="c5"><span class="c10">{{ $krs_mahasiswa->nm_ptk }}</span></p>
        </td>
    </tr>
    {{--*/ $total_sks += $krs_mahasiswa->sks_mk /*--}}
    @endforeach
    <tr class="c18">
        <td class="c39" colspan="3" rowspan="1">
            <p class="c6"><span class="c1">TOTAL SKS</span></p>
        </td>
        <td class="c28" colspan="1" rowspan="1">
            <p class="c6 c22"><span class="c1">{{ $total_sks }}</span></p>
        </td>
        <td class="c24" colspan="1" rowspan="1">
            <p class="c6 c22"><span class="c1"></span></p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c11 c22 c26"><span class="c15"></span></p>
<p class="c26 c11 c22"><span class="c15"></span></p>
<table class="c12">
    <tbody>
    <tr class="c14">
        <td class="c43" colspan="1" rowspan="1">
            <p class="c7"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 96.50px; height: 144.75px;"><img alt="2014100001.jpg" src="{{ URL::asset('assets/imgs/mhs_photo/2014100008.jpg') }}" style="width: 96.50px; height: 144.75px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
        </td>
        <td class="c33" colspan="1" rowspan="1">
            <p class="c6"><span class="c10">Wonosobo, @showToday </span></p>
            <p class="c6"><span class="c10">An.Direktur</span></p>
            <p class="c6"><span class="c10">Ketua Program Studi</span></p>
            <p class="c6 c22"><span class="c1"></span></p>
            <p class="c6 c22"><span class="c1"></span></p>
            <p class="c6 c22"><span class="c1"></span></p>
            <p class="c6 c22"><span class="c1"></span></p>
            <p class="c6"><span class="c15 c23 c37">{{ $mhs->kaprodi }}</span></p>
        </td>
    </tr>
    </tbody>
</table>
</body>

</html>