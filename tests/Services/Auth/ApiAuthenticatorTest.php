<?php

namespace Tests\Services\Auth;

use FE_UNSIQ\Eloquent\MahasiswaProfil;
use FE_UNSIQ\Eloquent\Role;
use FE_UNSIQ\Eloquent\SMS;
use FE_UNSIQ\Eloquent\User;
use FE_UNSIQ\Eloquent\Administrator;
use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Services\Auth\ApiAuthenticator;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\JWTManager;
use Tymon\JWTAuth\Token;

class ApiAuthenticatorTest extends TestCase
{

    use DatabaseMigrations;

    protected $apiAuthenticator;
    protected $credentials;
    protected $models = [];

    protected $validAdminPayloadData = [
        'username' => 'admin',
        'roles' =>  [
            'Administrator',
        ],
        'owner' =>  [
            'type' => 'Administrator',
            'name' => 'Fahri Baharudin',
        ],
    ];

    protected $validMahasiswaPayloadData = [
        'username' => 'fahri',
        'roles' =>  [
            'Mahasiswa',
        ],
        'owner' =>  [
            'type' => 'Mahasiswa',
            'name' => 'Mahasiswa Sukses',
            'id_reg_pd' => 'bd03498c-021f-4ad1-ab56-8b9d29a1dff1',
            'id_sms' => '83a0b8ad-8da3-4b38-b33e-7e849e801021',
            'nama_prodi' => 'Teknik Informatika',
            'semester_tempuh' => 8,
            'semester_aktif' => '20152'
        ],
    ];

    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $app = bootstrap_app_for_testing();
        $this->app = $app;

        return $app;
    }

    /**
     * Setting up the test class
     */
    public function setUp()
    {
        parent::setUp();
        $this->apiAuthenticator = $this->app->make(ApiAuthenticator::class);
        $this->credentials = [
            'username' => 'admin',
            'password' => 'admin'
        ];
    }

    /**
     * @throws \FE_UNSIQ\Services\Auth\ApiAuthenticatorException
     */
    public function testAuthenticateReturnValidToken()
    {
        $this->prepareTheDatabase();
        $token = $this->apiAuthenticator->authenticate($this->credentials);
        $payloadData = $this->decodeToken($token);

        $this->assertEquals($this->validAdminPayloadData, $payloadData);
    }

    /**
     * @expectedException \FE_UNSIQ\Services\Auth\ApiAuthenticatorException
     */
    public function testAuthenticateThrowingException()
    {
        $this->apiAuthenticator->authenticate(['username' => 'jhon', 'password' => 'secret']);
    }

    /**
     * @throws \FE_UNSIQ\Services\Auth\ApiAuthenticatorException
     */
    public function testGenerateTokenFromUserReturnValidToken()
    {
        $this->prepareTheDatabase();
        $laravelAuth = Mockery::mock(Guard::class);
        $laravelAuth->shouldReceive('check')->andReturn(true);
        $laravelAuth->shouldReceive('user')->andReturn($this->models['user']);
        $apiAuthenticator = new ApiAuthenticator($this->app->make(JWTAuth::class), $laravelAuth);
        $token = $apiAuthenticator->generateTokenFromUser();

        $this->assertEquals($this->validAdminPayloadData, $this->decodeToken($token));
    }

    /**
     * @throws \FE_UNSIQ\Services\Auth\ApiAuthenticatorException
     */
    public function testGenerateTokenFromUserWithUserTypeIsMahasiswa()
    {
        $this->prepareTheDatabase('Mahasiswa');
        $laravelAuth = Mockery::mock(Guard::class);
        $laravelAuth->shouldReceive('check')->andReturn(true);
        $laravelAuth->shouldReceive('user')->andReturn($this->models['user']);
        $apiAuthenticator = new ApiAuthenticator($this->app->make(JWTAuth::class), $laravelAuth);
        $token = $apiAuthenticator->generateTokenFromUser();

        $this->assertEquals($this->validMahasiswaPayloadData, $this->decodeToken($token));
    }

    /**
     * @expectedException \FE_UNSIQ\Services\Auth\ApiAuthenticatorException
     */
    public function testGenerateTokenFromUserThrowingException()
    {
        $this->apiAuthenticator->generateTokenFromUser();
    }

    /**
     * Decode a token
     *
     * @param Token $token
     * @return array
     */
    protected function decodeToken($token)
    {
        // JWT manager used to decode token
        $jwtManager = $this->app->make(JWTManager::class);
        $decoded = $jwtManager->decode(new Token($token))->toArray();
        $payloadData = $decoded['data'];

        return $payloadData;
    }

    /**
     * Prepare the database for testing usage
     * @param string $type
     */
    protected function prepareTheDatabase($type = 'Administrator')
    {
        $this->runDatabaseMigrations();

        Administrator::truncate();
        Role::truncate();
        User::truncate();

        if ($type == 'Administrator') {
            $this->models['administrator'] = Administrator::create(['name' => 'Fahri Baharudin']);
            $this->models['role'] = Role::create(['name' => 'Administrator']);
            $this->models['user'] = new User(['username' => 'admin', 'password' => 'admin', 'email' => 'jhon@doe.com']);
            $this->models['user']->owner()->associate($this->models['administrator']);
            $this->models['user']->save();
            $this->models['user']->roles()->attach($this->models['role']->id);
        } else if ($type == 'Mahasiswa') {
            $this->models['mahasiswa_profil'] = Mockery::mock(MahasiswaProfil::class);
            $this->models['mahasiswa_profil']->shouldReceive('getAttribute')->with('nm_pd')->andReturn('Mahasiswa Sukses');
            $this->models['sms'] = Mockery::mock(SMS::class);
            $this->models['sms']->shouldReceive('getAttribute')->with('id_sms')->andReturn('83a0b8ad-8da3-4b38-b33e-7e849e801021');
            $this->models['sms']->shouldReceive('getAttribute')->with('nm_lemb')->andReturn('Teknik Informatika');
            $this->models['mahasiswa'] = Mockery::mock(Mahasiswa::class);
            $this->models['mahasiswa']->shouldReceive('getAttribute')->with('id_reg_pd')->andReturn('bd03498c-021f-4ad1-ab56-8b9d29a1dff1');
            $this->models['mahasiswa']->shouldReceive('getAttribute')->with('mahasiswa_profil')->andReturn($this->models['mahasiswa_profil']);
            $this->models['mahasiswa']->shouldReceive('getAttribute')->with('sms')->andReturn($this->models['sms']);
            $this->models['mahasiswa']->shouldReceive('getSemesterTempuh')->andReturn(8);
            $this->models['mahasiswa']->shouldReceive('getThSemester')->andReturn('20152');
            $this->models['user'] = Mockery::mock(User::class);
            $this->models['user']->shouldReceive('getAttribute')->with('id')->andReturn(1);
            $this->models['user']->shouldReceive('getAttribute')->with('username')->andReturn('fahri');
            $this->models['user']->shouldReceive('getAttribute')->with('password')->andReturn('password');
            $this->models['user']->shouldReceive('getAttribute')->with('owner')->andReturn($this->models['mahasiswa']);
            $this->models['user']->shouldReceive('getAttribute')->with('owner_type')->andReturn(Mahasiswa::class);
            $this->models['user']->shouldReceive('listRoles')->andReturn(['Mahasiswa']);
        }
    }

}