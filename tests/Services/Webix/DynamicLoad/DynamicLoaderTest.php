<?php

namespace Tests\Services\Webix\DynamicLoad;

use DB;
use FE_UNSIQ\Eloquent\Administrator;
use FE_UNSIQ\Eloquent\Role;
use FE_UNSIQ\Eloquent\User;
use FE_UNSIQ\Services\Webix\DynamicLoad\Decorators\DefaultLimiter;
use FE_UNSIQ\Services\Webix\DynamicLoad\DynamicLoader;
use FE_UNSIQ\Services\Webix\DynamicLoad\DynamicLoadTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Http\Request;
use Mockery;

class DynamicLoaderTest extends TestCase
{

    use DatabaseMigrations;

    protected $models = [];
    protected $request;

    public function setUp()
    {
        parent::setUp();
        $this->prepareTheDatabase();
        $this->request = Mockery::mock(Request::class);
    }

    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $app = bootstrap_app_for_testing();
        $this->app = $app;

        return $app;
    }

    public function testMakeDynamicReturnValidBuilder()
    {
        $this->request->shouldReceive('has')->with('continue')->andReturn(FALSE);
        $this->request->shouldReceive('has')->with('start')->andReturn(FALSE);
        $this->request->shouldReceive('has')->with('count')->andReturn(FALSE);
        $this->request->shouldReceive('has')->with('filter')->andReturn(FALSE);

        $dynamicLoadTrait = new DynamicLoadTraitUserPrototype($this->request);
        $builder = $dynamicLoadTrait->makeDynamic(User::select());

        $this->assertEquals(Builder::class, get_class($builder));
    }

    public function testMakeDynamicForDefaultLimiter()
    {
        $builder = User::select('*');
        $this->setUpRequestOnHasMethod(['continue' => true, 'start' => false, 'count' => false, 'filter' => false]);
        $dynamicLoadTrait = new DynamicLoadTraitUserPrototype($this->request);
        $builder = $dynamicLoadTrait->makeDynamic($builder);
        $query = $this->getQueryLog($builder);

        $this->assertEquals('select * from "cms_users" limit 25', $query);
    }

    public function testMakeDynamicForSkipper()
    {
        $builder = User::select('*');
        $this->setUpRequestOnHasMethod(['continue' => false, 'start' => true, 'count' => false, 'filter' => false]);
        $this->request->shouldReceive('get')->with('start')->andReturn(2);
        $dynamicLoadTrait = new DynamicLoadTraitUserPrototype($this->request);
        $builder = $dynamicLoadTrait->makeDynamic($builder);
        $query = $this->getQueryLog($builder);

        $this->assertEquals('select * from "cms_users" limit 25 offset 1', $query);
    }

    public function testMakeDynamicForLimiter()
    {
        $builder = User::select('*');
        $this->setUpRequestOnHasMethod(['continue' => false, 'start' => false, 'count' => true, 'filter' => false]);
        $this->request->shouldReceive('get')->with('count')->andReturn(10);
        $dynamicLoadTrait = new DynamicLoadTraitUserPrototype($this->request);
        $builder = $dynamicLoadTrait->makeDynamic($builder);
        $query = $this->getQueryLog($builder);

        $this->assertEquals('select * from "cms_users" limit 10', $query);
    }

    public function testMakeDynamicForFilter()
    {
        $builder = User::select('*');
        $this->setUpRequestOnHasMethod(['continue' => false, 'start' => false, 'count' => false, 'filter' => true]);
        $this->request->shouldReceive('get')->with('filter')->andReturn(['username' => 'fahri']);
        $dynamicLoadTrait = new DynamicLoadTraitUserPrototype($this->request);
        $builder = $dynamicLoadTrait->makeDynamic($builder);
        $query = $this->getQueryLog($builder);

        $this->assertEquals('select * from "cms_users" where "username" LIKE ? limit 25', $query);
    }

    public function testGetRequestFromDecoratorReturnNotNull()
    {
        $dynamicLoader = new DynamicLoader($this->request);
        $dynamicLoader->setQueryBuilder(User::select());
        $dynamicLoadDecorator = new DefaultLimiter($dynamicLoader);

        $this->assertTrue( ! is_null($dynamicLoadDecorator->getRequest()));
    }

    protected function setUpRequestOnHasMethod(Array $array)
    {
        foreach ($array as $key => $value) {
            $this->request->shouldReceive('has')->with($key)->andReturn($value);
        }
    }

    protected function getQueryLog(Builder $builder)
    {
        DB::enableQueryLog();
        $builder->get();
        $query = DB::getQueryLog()[0]['query'];
        DB::flushQueryLog();

        return $query;
    }

    /**
     * Prepare the database for testing
     */
    protected function prepareTheDatabase()
    {
        $this->runDatabaseMigrations();

        Administrator::truncate();
        Role::truncate();
        User::truncate();

        $this->models['role'] = Role::create(['name' => 'Administrator']);

        $this->models['administrator'][1] = Administrator::create(['name' => 'Fahri Baharudin']);
        $this->models['user'][1] = new User(['username' => 'admin', 'password' => 'admin', 'email' => 'fahry@gmail.com']);
        $this->models['user'][1]->owner()->associate($this->models['administrator'][1]);
        $this->models['user'][1]->save();
        $this->models['user'][1]->roles()->attach($this->models['role']->id);

        $this->models['administrator'][2] = Administrator::create(['name' => 'Jhon Doe']);
        $this->models['user'][2] = new User(['username' => 'admin2', 'password' => 'admin2', 'email' => 'jhon@doe.com']);
        $this->models['user'][2]->owner()->associate($this->models['administrator'][2]);
        $this->models['user'][2]->save();
        $this->models['user'][2]->roles()->attach($this->models['role']->id);
    }

}

class DynamicLoadTraitUserPrototype
{
    protected $request;

    use DynamicLoadTrait;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}