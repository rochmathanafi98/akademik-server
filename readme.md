# API Server for akademik.feunsiq.ac.id
API Server supported by laravel, dingo-api, jwt-auth, fractal and many more!

### License
[MIT license](http://opensource.org/licenses/MIT)

## Installation:
- console -> composer install
- console -> php artisan migrate
- console -> php artisan db:seed
- console -> php artisan serve, run! :D

## ENDPOINTS:

### Auth
- GenerateToken (JWT) : POST -> /auth/generate-token
- Password Reset : PUT -> /auth/password-reset *form data: new_password

### Users
- List (DataTable) : GET -> /users
- Show : GET -> /users/{id_user}

### Mahasiswa
- List (DataTable) : GET -> /mahasiswa
- Show : GET -> /mahasiswa/{id_mahasiswa}
- Update : PUT -> /mahasiswa/{id_mahasiswa}
- Krs Diambil [List] : GET -> /mahasiswa/{id_mahasiswa}/krs
- Krs Diambil [Store] : POST -> /mahasiswa/{id_mahasiswa}/krs
- Krs Diambil [Show] : GET -> /mahasiswa/{id_mahasiswa}/krs/{krs_id}
- Krs Diambil [Destroy] : DELETE -> /mahasiswa/{id_mahasiswa}/krs/{krs_id}
- Krs Ditawarkan [List] : GET -> /mahasiswa/{id_mahasiswa}/krs-ditawarkan
- Krs Her [List] : GET -> /mahasiswa/{id_mahasiswa}/krs-her
- Nilai [List] (DataTable) : GET -> /mahasiswa/{id_mahasiswa}/nilai
- Riwayat Pendidikan [List] : GET -> /mahasiswa/{id_mahasiswa}/riwayat-pendidikan
- Kuliah Mahasiswa [List] : GET -> /mahasiswa/{id_mahasiswa}/kuliah-mahasiswa
- Status Registrasi [List] : GET -> /mahasiswa/{id_mahasiswa}/status-registrasi

### Dosen
- List (DataTable) : GET -> /dosen
- Show : GET -> /dosen/{dosen_id}
- Update : PUT -> /dosen/{dosen_id}
- Ajar Dosen [List] : GET -> /dosen/{dosen_id}/ajar-dosen *dosen_id = id_reg_ptk
- Ajar Dosen [Store] : POST -> /dosen/{dosen_id}/ajar-dosen
- Ajar Dosen [Delete] : DELETE -> /dosen/{dosen_id}/ajar-dosen/{ajar_dosen_id}
- Riwayat Penugasan Dosen [List] : GET -> /dosen/{dosen_id}/penugasan
- Riwayat Pendidikan Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-pendidikan
- Riwayat Struktural Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-stuktural
- Riwayat Fungsional Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-fungsional
- Riwayat Kepangkatan Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-kepangkatan
- Riwayat Sertifikasi Dosen [List] : GET -> /dosen/{dosen_id}/riwayat-sertifikasi

### MataKuliah
- List (DataTable) : GET -> /mata-kuliah
- Show : GET -> /mata-kuliah/{id_makul}
- Update : PUT -> /mata-kuliah/{id_makul}

### Kurikulum
- List (DataTable) : Get -> /kurikulum
- Update : PUT -> /kurikulum/{id_kurikulum}
- Mata Kuliah [List] : GET -> /kurikulum/{id_kurikulum}/mata-kuliah
- Mata Kuliah [Create] : POST -> /kurikulum/{id_kurikulum}/mata-kuliah
- Mata Kuliah [Show] : GET -> /kurikulum/{id_kurikulum}/mata-kuliah/{mata_kuliah_id}
- Mata Kuliah [Update] : PUT -> /kurikulum/{id_kurikulum}/mata-kuliah/{mata_kuliah_id}
- Mata Kuliah [Delete] : DELETE -> /kurikulum/{id_kurikulum}/mata-kuliah/{mata_kuliah_id}

### Semester
- List (DataTable) : GET -> /semester
- Kelas Kuliah [List] : GET -> /semester/{semester_id}/kelas-kuliah
- Kelas Kuliah [Store] : POST -> /semester/{semester_id}/kelas-kuliah
- Kelas Kuliah [Destroy] : DELETE -> /semester/{semester_id}/kelas-kuliah/{kelas_kuliah_id}
- Kuliah Mahasiswa [List] : GET -> /semester/{semester_id}/kuliah-mahasiswa

### Prodi
- List (DataTable) : GET -> /prodi
- Mata Kuliah [List] : GET -> /prodi/{prodi_id}/mata-kuliah
- Mata Kuliah [Show] : GET -> /prodi/{prodi_id}/mata-kuliah/{mata_kuliah_id}
- Mahasiswa Keluar (DO atau lulus) [List] : GET -> /prodi/{prodi_id}/mahasiswa-keluar
Todo: - Mahasiswa Keluar (DO atau lulus) [Store] : POST -> /prodi/{prodi_id}/mahasiswa-keluar
- Mahasiswa Keluar (DO atau lulus) [Show] : GET -> /prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa-keluar-id}
Todo: - Mahasiswa Keluar (DO atau lulus) [Update] : PUT -> /prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa-keluar-id}
- Mahasiswa Keluar (DO atau lulus) [Destroy] : DELETE -> /prodi/{prodi_id}/mahasiswa-keluar/{mahasiswa-keluar-id}

### KelasKuliah
- List (DataTable) : GET -> /kelas-kuliah
- Mahasiswa [List] : GET -> /kelas-kuliah/{id_kelas}/mahasiswa
- Dosen [List] : GET -> /kelas-kuliah/{id_kelas}/dosen
- Nilai [List] : GET -> /kelas-kuliah/{id_kelas}/nilai
- Nilai [Show] : GET -> /kelas-kuliah/{id_kelas}/nilai/{nilai_id}
- Nilai [Update] : PUT -> /kelas-kuliah/{id_kelas}/nilai/{nilai_id}

### Dashboard
- Jumlah Mahasiswa : GET -> /dashboard-semester/{semester_id}/jumlah-mahasiswa


'id_sms' => 'e8372392-ac46-491d-b200-735e053f18e8', 'id_smt' => '20152','id_mk' => 'af8b6f68-8221-4aea-b1c7-0919df9cfac3', 'nm_mk' => 'PENGANTAR ILMU EKONOMI', 'kode_mk' => 'FE202', 'sks_mk' => '3', 'nm_kls' => '02', 'kuota' => '50', 'tgl_mulai_koas' => '2015-09-01', 'tgl_selesai_koas' => '2020-09-01',