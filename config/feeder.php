<?php

return [
    'ws_sync' => env('SYNC_STATUS'),
    'ws_url' => env('FEEDER_URL', 'http://news.unsiq.ac.id:8082/ws/live.php?wsdl'),
    'ws_username' => env('FEEDER_USERNAME'),
    'ws_password' => env('FEEDER_PASSWORD')
];