<?php

namespace FE_UNSIQ\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Make a custom blade directive:
        \Blade::directive('indonesiaDate', function ($date) {
            $months = [
                'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
                'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
            ];
            $dates = explode('-', $date);
            return $dates[2] . ' ' . $months[intval($dates[1]) - 1] . ' ' . $dates[0];
        });

        \Blade::directive('showToday', function () {
            $date = date('Y-m-d');

            $months = [
                'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
                'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
            ];
            $dates = explode('-', $date);
            return $dates[2] . ' ' . $months[intval($dates[1]) - 1] . ' ' . $dates[0];
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
