<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;

/**
 * FE_UNSIQ\User
 *
 * @property-write mixed $password
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\FE_UNSIQ\Role[] $roles
 * @mixin \Eloquent
 */
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'password', 'owner_id', 'owner_type'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Setter for password, the password will automatically hashed
     * 
     * @param string $password 
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Setiap user memliliki owner (Polymorphic Relations)
     * @return mixed 
     */
    public function owner()
    {
        return $this->morphTo();
    }

    /**
     * belongsToMany to Role
     * @return mixed 
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'cms_user_role')->withTimestamps();
    }

    /**
     * List all the roles name to array
     *
     * @return array
     */
    public function listRoles()
    {
        $listRoles = [];

        foreach ($this->roles as $role) {
            $listRoles[] = $role->name;
        }

        return $listRoles;
    }

    /**
     * Check if user has role
     * @param  string  $name 
     * @return boolean       
     */
    public function hasRole($name)
    {
        foreach ($this->roles as $role) {
            if ($role->name == $name) return true;
        }

        return false;
    }

}
