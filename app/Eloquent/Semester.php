<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
	
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'semester';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_smt';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function kelas_kuliah()
	{
		return $this->hasMany(KelasKuliah::class, 'id_smt', 'id_smt');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function kuliah_mahasiswa()
	{
		return $this->hasMany(KuliahMahasiswa::class, 'id_smt', 'id_smt');
	}

}
