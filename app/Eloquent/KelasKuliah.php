<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class KelasKuliah extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'kelas_kuliah';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_kls';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    protected $fillable = [
        'id_kls',
        'id_sms',
        'id_smt',
        'id_mk',
        'nm_kls',
        'sks_mk',
        'sks_tm',
        'sks_prak',
        'sks_prak_lap',
        'sks_sim',
        'bahasan_case',
        'tgl_mulai_koas',
        'tgl_selesai_koas',
        'id_mou',
        'a_selenggara_pditt',
        'kuota_pditt',
        'a_pengguna_pditt',
        'id_kls_pditt',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mata_kuliah()
    {
        return $this->hasOne(MataKuliah::class, 'id_mk', 'id_mk');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function semester()
    {
        return $this->hasOne(Semester::class, 'id_smt', 'id_smt');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mata_kuliah_kurikulum()
    {
        return $this->hasOne(MataKuliahKurikulum::class, 'id_mk', 'id_mk');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nilai()
    {
        return $this->hasMany(Nilai::class, 'id_kls', 'id_kls');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ajar_dosen()
    {
        return $this->hasMany(AjarDosen::class, 'id_kls', 'id_kls');
    }
}
