<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenProfil extends Model
{

    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'dosen';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_ptk';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * HasOne Dosen
     * @return mixed
     */
    public function dosen()
    {
        return $this->hasOne(DosenPt::class, 'id_ptk');
    }

    public function ikatan_kerja_dosen()
    {
        return $this->belongsTo(IkatanKerjaDosen::class, 'id_ikatan_dosen');
    }

    /**
     * Retrieve agama yang terkait dengan model
     * @return mixed
     */
    public function agama()
    {
        return $this->belongsTo(Agama::class, 'id_agama');
    }

    /**
     * BelongsTo StatusKepegawaian
     * @return mixed
     */
    public function status_kepegawaian()
    {
        return $this->belongsTo(StatusKepagawaian::class, 'id_stat_pegawai');
    }

    /**
     * BelongsTo StatusKeaktifanPegawai
     * @return mixed
     */
    public function status_keaktifan_pegawai()
    {
        return $this->belongsTo(StatusKeaktifanPegawai::class, 'id_stat_aktif');
    }

    /**
     * BelongsTo Wilayah
     * @return mixed
     */
    public function wilayah()
    {
        return $this->belongsTo(Wilayah::class, 'id_wil');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riwayat_pendidikan()
    {
        return $this->hasMany(DosenRiwayatPendidikan::class, 'id_ptk', 'id_ptk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riwayat_struktural()
    {
        return $this->hasMany(DosenRiwayatStruktural::class, 'id_ptk', 'id_ptk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riwayat_fungsional()
    {
        return $this->hasMany(DosenRiwayatFungsional::class, 'id_ptk', 'id_ptk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riwayat_kepangkatan()
    {
        return $this->hasMany(DosenRiwayatKepangkatan::class, 'id_ptk', 'id_ptk');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riwayat_sertifikasi()
    {
        return $this->hasMany(DosenRiwayatSertifikasi::class, 'id_ptk', 'id_ptk');
    }
}
