<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    
	/**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'wilayah';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_wil';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;


	/**
	 * Retrieve mahasiswa yang terkait dengan model
	 * @return mixed 
	 */
	public function mahasiswa()
	{
		return $this->hasMany(Mahasiswa::class, 'id_wil');
	}
}
