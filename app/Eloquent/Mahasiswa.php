<?php

namespace FE_UNSIQ\Eloquent;

use FE_UNSIQ\Services\Semester\SemesterFactory as SemesterTranslator;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'mahasiswa_pt';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_reg_pd';

    /**
     * @var array
     */
    protected $fillable = [
        'id_reg_pd', 'id_pd', 'nipd', 'id_sms', 'id_sp', 'id_jns_daftar', 'tgl_masuk_sp'
    ];

    /**
     * Retrieve profil_mahasiswa yang terkait dengan model
     * @return mixed
     */
    public function mahasiswa_profil()
    {
        return $this->hasOne(MahasiswaProfil::class, 'id_pd', 'id_pd');
    }

    /**
     * Belongs to SMS
     * @return mixed
     */
    public function sms()
    {
        return $this->belongsTo(SMS::class, 'id_sms');
    }

    /**
     * MorphOne to: App\Eloquent\User
     * @return mixed
     */
    public function userAccount()
    {
        return $this->morphOne(User::class, 'owner', 'owner_type', 'owner_id', 'id');
    }

    /**
     * Has Many nilai
     * @return mixed
     */
    public function nilai()
    {
        return $this->hasMany(Nilai::class, 'id_reg_pd', 'id_reg_pd');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kuliah_mahasiswa()
    {
        return $this->hasMany(KuliahMahasiswa::class, 'id_reg_pd', 'id_reg_pd');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function status_registrasi()
    {
        return $this->hasMany(MahasiswaStatusRegistrasi::class, 'id_reg_pd', 'id_reg_pd');
    }

    /**
     * Get semester tempuh
     * @return int
     */
    public function getSemesterTempuh()
    {
        $semesterTranslator = new SemesterTranslator($this->mulai_smt);

        return $semesterTranslator->convertToSingle();
    }

    /**
     * JenisKeluar
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenis_keluar()
    {
        return $this->belongsTo(JenisKeluar::class, 'id_jns_keluar');
    }

    /**
     * JenisPendidikan
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenis_pendaftaran()
    {
        return $this->belongsTo(JenisPendaftaran::class, 'id_jns_daftar');
    }

    public function histori_pembayaran()
    {
        return $this->hasMany(HistoriPembayaran::class, 'id_reg_pd');
    }

}
