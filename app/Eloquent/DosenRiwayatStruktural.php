<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenRiwayatStruktural extends Model
{
    protected $table = 'dosen_struktural';
    protected $primaryKey = 'id_dos_struktural';
    public $timestamps = false;
}