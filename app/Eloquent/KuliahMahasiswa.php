<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class KuliahMahasiswa extends Model
{
    
	/**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'kuliah_mahasiswa';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_kuliah_pd';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * BelongsTo Mahasiswa
	 * @return mixed 
	 */
	public function mahasiswa()
	{
		return $this->belongsTo(Mahasiswa::class, 'id_reg_pd');
	}

	/**
	 * BelongsTo StatusMahasiswa
	 * @return mixed 
	 */
	public function status_mahasiswa()
	{
		return $this->belongsTo(StatusMahasiswa::class, 'id_stat_mhs');
	}
}
