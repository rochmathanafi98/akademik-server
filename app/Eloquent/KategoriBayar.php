<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class KategoriBayar extends Model
{
    protected $table = 'kategori_bayar';
    protected $primaryKey = 'id_kategori_bayar';
    public $timestamps = false;

    protected $fillable = [
        'id_kategori_bayar', 'nm_kategori_bayar', 'ket_kategori_bayar'
    ];

    public function histori_pembayaran()
    {
        return $this->belongsTo(HistoriPembayaran::class, 'id_kategori_bayar');
    }
}