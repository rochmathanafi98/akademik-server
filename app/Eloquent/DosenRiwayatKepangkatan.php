<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenRiwayatKepangkatan extends Model
{
    protected $table = 'dosen_kepangkatan';
    protected $primaryKey = 'id_dos_pangkat';
    public $timestamps = false;
}