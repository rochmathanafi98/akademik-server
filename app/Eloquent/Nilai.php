<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed kelas_kuliah
 * @property mixed mahasiswa
 */
class Nilai extends Model
{
    /**
     * Database table yang berhubungan dengan Model
	 *
     * @var string
     */
	protected $table = 'nilai';

	/**
	 * Primary key pada table
	 *
	 * @var string
	 */
	protected $primaryKey = 'id_nilai';

	protected $fillable = [
		'id_nilai',
		'id_kls',
		'id_reg_pd',
		'asal_data',
		'nilai_angka',
		'nilai_huruf',
		'nilai_index',
        'id_mk',
        'nm_mk',
        'smt',
        'sks'
	];

	/**
	 * Disable timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Belongs to many KelasKuliah
	 *
	 * @return mixed 
	 */
	public function kelas_kuliah()
	{
		return $this->belongsTo(KelasKuliah::class, 'id_kls');
	}

	/**
	 * Belongs to mahasiswa
	 *
	 * @return mixed 
	 */
	public function mahasiswa()
	{
		return $this->belongsTo(Mahasiswa::class, 'id_reg_pd');
	}


	/**
	 * Hitung semester dari nilai
	 *
	 * @return float
     */
	public function getSemester()
	{
		$semester = intval($this->kelas_kuliah->id_smt);
		$angkatan = intval(substr($this->mahasiswa->mulai_smt, 0, 4));

		if ($semester % 2 != 0) {
			$a = (($semester + 10) - 1) / 10;
			$b = $a - $angkatan;
			$c = ($b * 2) - 1;
		} else {
			$a = (($semester + 10) - 2) / 10;
			$b = $a - $angkatan;
			$c = $b * 2;
		}

		return $c;
	}
}
