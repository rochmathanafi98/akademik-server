<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class SMS extends Model
{
    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;
    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'sms';
    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_sms';

    /**
     * Has Many mahasiswa
     * @return mixed
     */
    public function mahasiswa()
    {
        return $this->hasMany(Mahasiswa::class, 'id_sms');
    }

    /**
     * Has Many mata_kuliah
     * @return mixed
     */
    public function mata_kuliah()
    {
        return $this->hasMany(MataKuliah::class, 'id_sms');
    }

    /**
     * Has Many dosen_pt
     * @return mixed
     */
    public function dosen_pt()
    {
        return $this->hasMany(DosenPt::class, 'id_sms');
    }

    public function bobot_nilai()
    {
        return $this->hasMany(BobotNilai::class, 'id_sms');
    }
}
