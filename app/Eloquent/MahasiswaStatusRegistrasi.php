<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MahasiswaStatusRegistrasi extends Model
{
    protected $table = 'registrasi_mahasiswa';
    protected $primaryKey = 'id_registrasi';
    public $timestamps = false;
}