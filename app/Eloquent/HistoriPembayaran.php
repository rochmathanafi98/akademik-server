<?php

namespace FE_UNSIQ\Eloquent;

use FE_UNSIQ\Eloquent\Mahasiswa;
use Illuminate\Database\Eloquent\Model;

class HistoriPembayaran extends Model
{
    protected $table = 'histori_pembayaran';
    protected $primaryKey = 'id_pembayaran';
    public $timestamps = false;

    protected $fillable = [
        'id_pembayaran', 'tgl_bayar', 'id_kategori_bayar', 'tags', 'jumlah_bayar', 'id_smt', 'smt', 'id_reg_pd', 'id_sms'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kategori_bayar()
    {
        return $this->hasMany(KategoriBayar::class, 'id_kategori_bayar');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'id_reg_pd');
    }
}