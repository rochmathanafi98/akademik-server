<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * FE_UNSIQ\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\FE_UNSIQ\Eloquent\User[] $user
 * @mixin \Eloquent
 */
class Role extends Model
{
	protected $table = 'cms_roles';
	protected $fillable = ['name'];

	public function user()
	{
		return $this->belongsToMany(User::class, 'cms_user_role')->withTimestamps();
	}
}
