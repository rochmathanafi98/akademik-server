<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenPt extends Model
{

    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'dosen_pt';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_reg_ptk';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * BelongsTo DosenProfil
     * @return mixed
     */
    public function dosen_profil()
    {
        return $this->belongsTo(DosenProfil::class, 'id_ptk');
    }

    /**
     * BelongsTo SMS
     * @return mixed
     */
    public function program_studi()
    {
        return $this->belongsTo(SMS::class, 'id_sms');
    }

    /**
     * HasMany AjarDosen
     * @return mixed
     */
    public function ajar_dosen()
    {
        return $this->hasMany(AjarDosen::class, 'id_reg_ptk');
    }

    /**
     * HasMany TahunAjaran
     * @return mixed
     */
    public function tahun_ajaran()
    {
        return $this->belongsTo(TahunAjaran::class, 'id_thn_ajaran');
    }
}
