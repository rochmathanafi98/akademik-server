<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MahasiswaProfil extends Model
{
    
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'mahasiswa';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_pd';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;


	/**
	 * @var array
	 */
	protected $fillable = [
		'id_pd', 'nm_pd', 'jk', 'tgl_lahir', 'tmpt_lahir',
		'id_kk', 'id_sp', 'stat_pd', 'ds_kel', 'id_wil', 'kewarganegaraan',
		'nm_ibu_kandung', 'id_agama', 'id_kebutuhan_khusus_ayah', 'id_kebutuhan_khusus_ibu',
		'a_terima_kps',
	];

	/**
	 * Retrieve mahasiswa yang terkait dengan model
	 * @return mixed
	 */
	public function mahasiswa()
	{
		return $this->belongsTo(Mahasiswa::class, 'id_pd', 'id_pd');
	}

	/**
	 * Retrieve status_mahasiswa yang terkait dengan model
	 * @return mixed 
	 */
	public function status_mahasiswa()
	{
		return $this->belongsTo(StatusMahasiswa::class, 'stat_pd', 'id_stat_mhs');
	}

	/**
	 * Retrieve wilayah yang terkait dengan model
	 * @return mixed 
	 */
	public function wilayah()
	{
		return $this->belongsTo(Wilayah::class, 'id_wil', 'id_wil');
	}

	/**
	 * Retrieve agama yang terkait dengan model
	 * @return mixed 
	 */
	public function agama()
	{
		return $this->belongsTo(Agama::class, 'id_agama');
	}
}
