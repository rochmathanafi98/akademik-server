<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenRiwayatSertifikasi extends Model
{
    protected $table = 'dosen_sertifikasi';
    protected $primaryKey = 'id_dos_sert';
    public $timestamps = false;
}