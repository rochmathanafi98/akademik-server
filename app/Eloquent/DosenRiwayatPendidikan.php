<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenRiwayatPendidikan extends Model
{
    protected $table = 'dosen_pendidikan';
    protected $primaryKey = 'id_dos_pendidikan';
    public $timestamps = false;
}