<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'tahun_ajaran';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_thn_ajaran';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = false;

    /**
     * hasMany dosen
     * @return mixed
     */
    public function dosen()
    {
        return $this->hasMany(DosenPt::class, 'id_thn_ajaran');
    }

}
