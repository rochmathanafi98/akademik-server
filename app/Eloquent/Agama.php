<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{

    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'agama';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_agama';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;

	/**
	 * Retrieve mahasiswa yang terkait dengan model
	 * @return mixed
	 */
	public function mahasiswa()
	{
		return $this->hasMany(MahasiswaProfil::class, 'id_agama');
	}


    public function dosen()
    {
        return $this->hasMany(DosenProfil::class, 'id_agama');
    }


}
