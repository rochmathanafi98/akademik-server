<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class AjarDosen extends Model
{

    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'ajar_dosen';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_ajar';

	protected $fillable = [
		'id_ajar', 'id_subst', 'id_kls', 'sks_subst_tot', 'sks_tm_subst', 'sks_lab_prak_subst', 'sks_sim_subst',
		'jml_tm_renc', 'jml_tm_real'
	];

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;

	/**
	 * BelongsTo Dosen
	 * @return mixed 
	 */
	public function dosen()
	{
		return $this->belongsTo(DosenPt::class, 'id_reg_ptk');
	}

	/**
	 * BelongsTo KelasKulian
	 * @return mixed 
	 */
	public function kelas_kuliah()
	{
		return $this->belongsTo(KelasKuliah::class, 'id_kls');
	}

}
