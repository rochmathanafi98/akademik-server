<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class DosenRiwayatFungsional extends Model
{
    protected $table = 'dosen_jabfung';
    protected $primaryKey = 'id_dos_jabfung';
    public $timestamps = false;
}