<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class StatusKepagawaian extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
	protected $table = 'status_kepegawaian';

	/**
	 * Primary key pada table
	 * @var string
	 */
	protected $primaryKey = 'id_stat_pegawai';

	/**
	 * Disable timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Disable increment primary key, karena primary key menggunakan varchar
	 * @var boolean
	 */
	public $incrementing = false;
}
