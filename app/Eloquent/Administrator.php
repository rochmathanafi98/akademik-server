<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * FE_UNSIQ\Administrator
 *
 * @property-read \FE_UNSIQ\User $userAccount
 * @mixin \Eloquent
 */
class Administrator extends Model
{
    protected $table = 'cms_administrator';
    protected $fillable = ['name'];
    
    public function userAccount() 
    {
        return $this->morphOne(User::class, 'owner', 'owner_type', 'owner_id', 'id');
    }
}
