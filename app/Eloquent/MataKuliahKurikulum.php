<?php

namespace FE_UNSIQ\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MataKuliahKurikulum extends Model
{
    /**
     * Database table yang berhubungan dengan Model
     * @var string
     */
    protected $table = 'mata_kuliah_kurikulum';

    /**
     * Primary key pada table
     * @var string
     */
    protected $primaryKey = 'id_mk_kurikulum';

    /**
     * Disable timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Disable increment primary key, karena primary key menggunakan varchar
     * @var boolean
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = [
        'id_kurikulum_sp',
        'id_mk',
        'smt',
        'sks_mk',
        'sks_tm',
        'sks_prak',
        'sks_prak_lab',
        'sks_sim',
        'a_wajib'
    ];
}