<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;
use Illuminate\Support\Facades\Input;

class MataKuliahKurikulumRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //to debug sql query
        /*\DB::listen(function($sql, $bindings, $time) {
            var_dump($sql);
            var_dump($bindings);
            var_dump($time);
        });*/

        return [
            'id_kurikulum_sp' => 'required',
            'id_mk' => 'required',
            'sks_mk' => 'required',
//            'smt' => 'unique:mata_kuliah_kurikulum,smt,NULL,id_mk_kurikulum,id_mk,'.Input::get('id_mk').',id_kurikulum_sp,'.Input::get('id_kurikulum_sp'),

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Matakuliah Kurikulum Gagal di simpan');
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
