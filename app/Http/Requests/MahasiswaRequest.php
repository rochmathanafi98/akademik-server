<?php

namespace FE_UNSIQ\Http\Requests;

class MahasiswaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nm_pd' => 'required',
            'jk' => 'required',
//            'nisn' => 'required',
            'nik' => 'required',
            'tmpt_lahir' => 'required',
            'tgl_lahir' => 'required',
//            'id_agama' => 'required',
//            'id_kk' => 'required',
//            'id_sp' => 'required',
//            'jln' => 'required',
//            'rt' => 'required',
//            'rw' => 'required',
//            'nm_dsn' => 'required',
//            'ds_kel' => 'required',
//            'id_wil' => 'required',
//            'kode_pos' => 'required',
//            'telepon_seluler' => 'required',
//            'nm_ayah' => 'required',
//            'nm_ibu_kandung' => 'required',
//            'kewarganegaraan' => 'required',

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages("Mahasiswa gagal disimpan");
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
