<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class DosenRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_ikatan_kerja' => 'required',
            'nm_ptk' => 'required',
            'nip' => 'required',
            'jk' => 'required',
            'tmpt_lahir' => 'required',
            'tgl_lahir' => 'required',
            'nik' => 'required',
            'id_agama' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'nm_dsn' => 'required',
            'ds_kel' => 'required',
            'id_wil' => 'required',
            'kode_pos' => 'required',
            'no_hp' => 'required',
            'id_sp' => 'required',
            'sk_angkat' => 'required',
            'tmt_sk_angkat' => 'required',
            'nm_ibu_kandung' => 'required',
            'stat_kawin' => 'required',
            'kewarganegaraan' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Dosen Gagal di simpan');
        return [
            'id_ptk.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
