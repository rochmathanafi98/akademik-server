<?php

namespace FE_UNSIQ\Http\Requests;

//use Dingo\Api\Http\FormRequest;
//use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\ResourceException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

abstract class Request extends FormRequest
{
    /**
     * Override the failedValidation method in order to avoid redirection
     *
     * and return a valid api validation error
     *
     * @param Validator $validator
     * @throws ResourceException
     */

    protected $throwMessages ;

    protected function failedValidation(Validator $validator) {
        throw new ResourceException($this->throwMessages, $validator->errors());
    }

    public function setThrowMessages($message){
        $this->throwMessages = $message;
    }

}
