<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class MahasiswaPtRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_sms' => 'required',
            'id_pd' => 'required',
            'id_sp' => 'required',
            'id_jns_daftar' => 'required',
            'nipd' => 'required',
            'tgl_masuk_sp' => 'required',
            'mulai_smt' => 'required',

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Mahasiswa PT Gagal di simpan');
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
