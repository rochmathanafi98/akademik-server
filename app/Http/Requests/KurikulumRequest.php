<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class KurikulumRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nm_kurikulum_sp' => 'required',
            'jml_sem_normal' => 'required',
            'jml_sks_lulus' => 'required',
            'jml_sks_wajib' => 'required',
            'jml_sks_pilihan' => 'required',
            'id_sms' => 'required',
            'id_jenj_didik' => 'required',
            'id_smt_berlaku' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Kurikulum Gagal di simpan');
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
