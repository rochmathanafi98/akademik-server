<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class DosenPtRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_ptk' => 'required',
            'id_sp' => 'required',
            'id_thn_ajaran' => 'required',
            'id_sms' => 'required',
            'no_srt_tgs' => 'required',
            'tgl_srt_tgs' => 'required',
            'tmt_srt_tgs' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Dosen PT Gagal di simpan');
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
