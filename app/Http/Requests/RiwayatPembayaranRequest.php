<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class RiwayatPembayaranRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_kategori_bayar' => 'required',
            'tags' => 'required',
            'jumlah_bayar' => 'required',
            'id_smt' => 'required',
            'smt' => 'required',
            'id_sms' => 'required',
        ];
    }
}
