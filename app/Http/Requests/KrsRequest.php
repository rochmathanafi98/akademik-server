<?php

namespace FE_UNSIQ\Http\Requests;

use Illuminate\Support\Facades\Input;

class KrsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //TODO: KRS HANYA INSERT DAN DELETE TABEL NILAI
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //to debug sql query
        /*\DB::listen(function($sql, $bindings, $time) {
             var_dump($sql);
             var_dump($bindings);
             var_dump($time);
         });*/

        if ($this->segment(1) == 'kelas-kuliah') {
            return [
                'id_kls' => 'required|unique:nilai,id_kls,NULL,id_nilai,id_reg_pd,' . Input::get('id_reg_pd'),
                'id_mk' => 'unique:nilai,id_mk,NULL,id_nilai,id_reg_pd,' . Input::get('id_mk'),
                'id_reg_pd' => 'required|exists:histori_pembayaran,id_reg_pd,id_smt,'.Input::get('id_smt').',id_kategori_bayar,1',
                'id_sms' => 'required',
            ];
        }

        return [
            'id_reg_pd' =>'required|exists:histori_pembayaran,id_reg_pd,id_smt,'.Input::get('id_smt').',id_kategori_bayar,1',
            'id_kls' => 'required|unique:nilai,id_kls,NULL,id_nilai,id_reg_pd,' . Input::get('id_reg_pd'),
            'id_mk' => 'required|unique:nilai,id_mk,NULL,id_nilai,id_reg_pd,' . Input::get('id_mk'),
            'id_smt' => 'required',
            'kode_mk' => 'required',
            'sks_mk' => 'required',
            'smt' => 'required',
            'id_sms' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('KRS Gagal di simpan');
        return [
            'id_kls.required' => ':attribute Tidak boleh kosong',
            'id_reg_pd.exists' =>'Belum membayar SPP di semester '.Input::get('id_smt')
        ];
    }
}
