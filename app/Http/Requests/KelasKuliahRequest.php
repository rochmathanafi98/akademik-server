<?php

namespace FE_UNSIQ\Http\Requests;

use FE_UNSIQ\Http\Requests\Request;

class KelasKuliahRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id_sms' => 'required',
            'id_smt' => 'required',
            'nm_kls' => 'required',
            'id_mk' => 'required',
            'sks_mk' => 'required',
            'tgl_mulai_koas' => 'required',
            'tgl_selesai_koas' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $this->setThrowMessages('Kelas Kuliah Gagal di simpan');
        return [
//            'a_bahan_ajar.required' => ':attribute Tidak boleh kosong',
        ];
    }
}
