<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Http\Requests\MahasiswaRequest;
use FE_UNSIQ\Jobs\Mahasiswa\CreateSyncMahasiswa;
use FE_UNSIQ\Jobs\Mahasiswa\UpdateMahasiswa;
use FE_UNSIQ\Repositories\Mahasiswa\MahasiswaRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MahasiswaController extends Controller
{

    /**
     * @var MahasiswaRepository
     */
    protected $mahasiswaRepo;

    /**
     * MahasiswaController constructor.
     * @param MahasiswaRepository $mahasiswaRepo
     */
    public function __construct(MahasiswaRepository $mahasiswaRepo)
    {
        $this->mahasiswaRepo = $mahasiswaRepo;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function mahasiswaIndex()
    {
        $mhsResource = $this->mahasiswaRepo->getAll();
        $mhsResponse = $this->responseWebixCollection($mhsResource, function ($mhs) {
            return [
                'id' => $mhs->id_reg_pd,
                'id_reg_pd' => $mhs->id_reg_pd,
                'nm_pd' => $mhs->nm_pd,
                'nipd' => $mhs->nipd,
                'jk' => $mhs->jk,
                'tmpt_lahir' => $mhs->tmpt_lahir,
                'tgl_lahir' => $mhs->tgl_lahir,
                'nm_stat_mhs' => $mhs->nm_stat_mhs,
                'id_sms' => $mhs->id_sms,
                'nm_prodi' => $mhs->nm_lemb
            ];
        });

        return $mhsResponse;
    }

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Http\Response
     */
    public function mahasiswaShow($mahasiswa_id)
    {
        try {
            $mhs = $this->mahasiswaRepo->show($mahasiswa_id);
            return $this->responseWebixItem($mhs);
        } catch (ModelNotFoundException $e) {
            $response = $this->response();
            $response->errorNotFound('Mahasiswa dengan id_reg_pd \'' . $mahasiswa_id . '\' tidak ditemukan');
            return $response;
        }
    }

    /**
     * @return \Dingo\Api\Http\Response|void
     */
    public function mahasiswaStore(MahasiswaRequest $mahasiswaRequest)
    {

        // executing some jobs
        $createSyncMhsJob = new CreateSyncMahasiswa($mahasiswaRequest->all());
        $jobResult = $this->dispatch($createSyncMhsJob);

        // return the response
        if ($jobResult) {
            return $this->responseWebixCreate($jobResult, function ($mhs) {
                return [
                    'id' => $mhs->id_pd,
                    'id_reg_pd' => $mhs->regpd_id_reg_pd
                ];
            });
        }

        return $this->response->error('Mahasiswa gagal ditambahkan', 422);
    }

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Http\Response|void
     */
    public function mahasiswaUpdate(MahasiswaRequest $mahasiswaRequest, $mahasiswa_id)
    {
        if ($this->dispatch(new UpdateMahasiswa($mahasiswa_id, $mahasiswaRequest->all()))) {

            $mhs = $this->mahasiswaRepo->show($mahasiswa_id);
            $responseUpdated = $this->responseWebixUpdate($mhs, function ($mhs) {
                return [
                    'id' => $mhs->id_reg_pd,
                ];
            });

            return $responseUpdated;
        }

        return $this->response->error('Mahasiswa gagal diupdate', 422);
    }

    public function mahasiswaGrafik()
    {
        $mhsResource = $this->mahasiswaRepo->getGrafikMahasiswa();
        $mhsResponse = $this->responseWebixChart($mhsResource);
        return $mhsResponse;
    }
}
