<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

use Illuminate\Database\Eloquent\Collection;

class StatusRegistrasiController extends MahasiswaController
{

    /**
     * @param $mahasiswa_id
     * @return Collection
     */
    public function statusRegistrasiIndex($mahasiswa_id)
    {
        unset($mahasiswa_id);

        return new Collection([]);
    }
    
}