<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

use Carbon\Carbon;
use FE_UNSIQ\Eloquent\HistoriPembayaran;
use FE_UNSIQ\Http\Requests\RiwayatPembayaranRequest;
use Illuminate\Http\Request;

class RiwayatPembayaranController extends MahasiswaController
{
    /**
     * @param $id_reg_pd
     * @return \Illuminate\Http\Response
     */
    public function riwayatPembayaranIndex($id_reg_pd)
    {
        $historiPembayaranResource = $this->mahasiswaRepo->getHistoriPembayaran($id_reg_pd);

        return $this->responseWebixCollection($historiPembayaranResource);
    }

    /**
     * @param $id_reg_pd
     * @return \Dingo\Api\Http\Response|void
     */
    public function riwayatPembayaranStore(RiwayatPembayaranRequest $riwayatPembayaranRequest, $id_reg_pd)
    {
        $dataBayar = $riwayatPembayaranRequest->all();
        $dataBayar['id_reg_pd'] = $id_reg_pd;
        $dataBayar['tgl_bayar'] = (new Carbon())->now();

        if ($respnse = HistoriPembayaran::create($dataBayar)) {
            return $this->responseWebixCreate($respnse);
        }

        return $this->response->error('Gagal mencatat histori pembayaran', 422);
    }
}