<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\Nilai;
use FE_UNSIQ\Eloquent\NilaiArsip;
use FE_UNSIQ\Http\Requests\KrsRequest;
use FE_UNSIQ\Http\Requests\NilaiRequest;
use FE_UNSIQ\Jobs\Mahasiswa\UpdateNilai;
use FE_UNSIQ\Services\Nilai\NilaiCalculator;
use FE_UNSIQ\Services\Semester\SemesterFactory;

class NilaiController extends MahasiswaController
{

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Http\Response
     */
    public function nilaiIndex($mahasiswa_id)
    {
        $allNilaiFromMahasiswa = $this->mahasiswaRepo->getKrs($mahasiswa_id);

        // return $this->responseWebixCollection($allNilaiFromMahasiswa);

        $nilaiResponse = $this->responseWebixCollection($allNilaiFromMahasiswa, function ($nilai) {
            return [
                'id' => $nilai->id_nilai,
                'a_wajib' => $nilai->a_wajib,
                'id_kls' => $nilai->id_kls,
                'id_kurikulum_sp' => $nilai->id_kurikulum_sp,
                'id_mk' => $nilai->id_mk,
                'id_mk_kurikulum' => $nilai->id_mk_kurikulum,
                'id_reg_pd' => $nilai->id_reg_pd,
                'id_sms' => $nilai->id_sms,
                'id_smt' => $nilai->id_smt,
                'kode_mk' => $nilai->kode_mk,
                'nilai_angka' => $nilai->nilai_angka,
                'nilai_huruf' => $nilai->nilai_huruf,
                'nilai_indeks' => $nilai->nilai_indeks,
                'nm_kls' => $nilai->nm_kls,
                'nm_mk' => $nilai->nm_mk,
                'sks_mk' => $nilai->sks_mk,
                'sks_prak' => $nilai->sks_prak,
                'sks_prak_lap' => $nilai->sks_prak_lap,
                'sks_sim' => $nilai->sks_sim,
                'sks_tm' => $nilai->sks_tm,
                'semester' => $nilai->smt,
                'asal_data' => $nilai->asal_data,
            ];
        });

        return $nilaiResponse;
    }


    /**
     * store krs dari sisi mahasiswa
     * @param KrsRequest $krsRequest
     * @param $mahasiswa_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function nilaiStore(KrsRequest $krsRequest, $mahasiswa_id)
    {

        //if her TODO: belum di test
        if ($krsRequest->has('is_her')) {
            $nilaiOld = Nilai::findOrFail($krsRequest->get('id_nilai'));
            NilaiArsip::create($nilaiOld->toArray()); //FIXME: pakenya replace (insert new item if not exist, otherwise update)

            /*$nilai->nilai_huruf = '';
            $nilai->nilai_angka = '';
            $nilai->nilai_indeks = '';
            $nilai->is_her = 1;*/

            $requestData['id_reg_pd'] = $mahasiswa_id;
            $requestData = $krsRequest->all();
            $mahasiswa = Mahasiswa::findOrFail($mahasiswa_id);

            $nilai = new Nilai($requestData);

            if ($mahasiswa->nilai()->save($nilai)) {
                return response()->json(['id' => $nilai->id_nilai])->setStatusCode(201);
            }

            return $this->response()->error('Terjadi kesalahan', 422);
        }

        $requestData['id_reg_pd'] = $mahasiswa_id;
        $requestData = $krsRequest->all();

        $mahasiswa = Mahasiswa::findOrFail($mahasiswa_id);

        $nilai = new Nilai($requestData);
        $mahasiswa->nilai()->save($nilai);

        return $this->responseWebixCreate($nilai, function ($nilai) {
            return [
                'id' => $nilai->id_nilai
            ];
        });
    }


    public function nilaiShow($mahasiswa_id, $nilai_id)
    {
        $nilaiItem = $this->mahasiswaRepo->showKrs($mahasiswa_id, $nilai_id);

        return $this->responseWebixItem($nilaiItem);
    }

    public function hssCetak($mahasiswa_id, $smt_mahasiswa)
    {
        $nilaiFromMahasiswaSemester = $this->mahasiswaRepo->getKrs($mahasiswa_id, $smt_mahasiswa);
        $nilaiSemester = $nilaiFromMahasiswaSemester->getItems();

        $allNilaiFromMahasiswa = $this->mahasiswaRepo->getKrs($mahasiswa_id);
        $nilaiKumulatif = $allNilaiFromMahasiswa->getItems();

        $biodataMahasiswa = $this->mahasiswaRepo->show($mahasiswa_id);
        $biodataMahasiswa->smt_mhs = (new SemesterFactory($biodataMahasiswa->mulai_smt))->convertToSingle();

        return view('pdf.hss', [
                'semester_tempuh' => $smt_mahasiswa,
                'nilai' => $nilaiSemester,
                'ips' => (new NilaiCalculator($nilaiSemester))->getIPK(),
                'ipk' => (new NilaiCalculator($nilaiKumulatif))->getIPK(),
                'mhs' => $biodataMahasiswa]
        );


        return view('pdf.hss', ['krs' => $allKrsFromMahasiswa->getItems()]);
    }

    public function transkripCetak($mahasiswa_id)
    {
        $allNilaiFromMahasiswa = $this->mahasiswaRepo->getKrs($mahasiswa_id);
        $biodataMahasiswa = $this->mahasiswaRepo->show($mahasiswa_id);
        $biodataMahasiswa->smt_mhs = (new SemesterFactory($biodataMahasiswa->mulai_smt))->convertToSingle();
        $nilai = $allNilaiFromMahasiswa->getItems();

        return view('pdf.transkrip', [
                'nilai' => $nilai,
                'ipk' => (new NilaiCalculator($nilai))->getIPK(),
                'mhs' => $biodataMahasiswa]
        );

    }

    public function nilaiUpdate(NilaiRequest $nilaiRequest, $mahasiswa_id, $nilai_id)
    {

        $job = new UpdateNilai($nilai_id, $nilaiRequest->all());

        if ($nilaiJob = $this->dispatch($job)) {
            $responseUpdated = $this->responseWebixUpdate($nilaiJob, function ($nilai) {
                return [
                    'id' => $nilai->id_nilai,
                    'nilai_angka' => $nilai->nilai_angka,
                    'nilai_indeks' => $nilai->nilai_indeks,
                    'nilai_huruf' => $nilai->nilai_huruf,
                ];
            });

            return $responseUpdated;
        }

        return $this->response->error('Terjadi kesalahan', 500);
    }

    public function nilaiDestroy($mahasiswa_id, $nilai_id)
    {
        $mahasiswa = Mahasiswa::findOrFail($mahasiswa_id);
        $nilai = $mahasiswa->nilai()->where('id_nilai', '=', $nilai_id)->first();

        if ($nilai->delete()) {
            return $this->response()->accepted();
        }

        return $this->response()->error('Terjadi kesalahan', 500);
    }

}
