<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

class KuliahMahasiswaController extends MahasiswaController
{

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Http\Response
     */
    public function kuliahMahasiswaIndex($mahasiswa_id)
    {
        $kuliahMahasiswa = $this->mahasiswaRepo->getKuliahMahasiswa($mahasiswa_id);

        return $this->responseWebixCollection($kuliahMahasiswa);
    }

}