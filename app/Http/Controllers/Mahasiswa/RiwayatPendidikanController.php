<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

class RiwayatPendidikanController extends MahasiswaController
{

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Http\Response
     */
    public function riwayatPendidikanIndex($mahasiswa_id)
    {
        $riwayatPendidikan = $this->mahasiswaRepo->getRiwayatPendidikan($mahasiswa_id);

        return $this->responseWebixCollection($riwayatPendidikan);
    }
    
}