<?php

namespace FE_UNSIQ\Http\Controllers\Mahasiswa;

use FE_UNSIQ\Http\Requests\KrsRequest;
use FE_UNSIQ\Jobs\Nilai\CreateNilaiFromMahasiswa;
use FE_UNSIQ\Jobs\Nilai\DestroyNilaiFromMahasiswa;
use FE_UNSIQ\Services\Semester\SemesterFactory;

class KrsController extends MahasiswaController
{

    /**
     * @param $mahasiswa_id
     * @return \Illuminate\Http\Response
     */
    public function krsIndex($mahasiswa_id)
    {
        $allKrsFromMahasiswa = $this->mahasiswaRepo->getKrs($mahasiswa_id);

        return $this->responseWebixCollection($allKrsFromMahasiswa, function ($krs) {
            return [
                'id' => $krs->id_nilai,
                'id_kls' =>$krs->id_kls,
                'id_mk' =>$krs->id_mk,
                'id_reg_pd' =>$krs->id_reg_pd,
                'id_sms' =>$krs->id_sms,
                'id_smt' =>$krs->id_smt,
                'kode_mk' =>$krs->kode_mk,
                'nm_mk' =>$krs->nm_mk,
                'is_her' =>$krs->is_her,
                'nilai_angka' =>$krs->nilai_angka,
                'nilai_huruf' =>$krs->nilai_huruf,
                'nilai_indeks' =>$krs->nilai_indeks,
                'nipd' =>$krs->nipd,
                'nm_kls' =>$krs->nm_kls,
                'nm_pd' =>$krs->nm_pd,
                'sks' =>$krs->sks,
                'sks_mk' =>$krs->sks_mk,
                'smt' =>$krs->smt,
            ];
        });
    }

    public function krsCetak($mahasiswa_id)
    {
        $allKrsFromMahasiswa = $this->mahasiswaRepo->getKrs($mahasiswa_id);
        $biodataMahasiswa = $this->mahasiswaRepo->show($mahasiswa_id);
        $biodataMahasiswa->smt_mhs = (new SemesterFactory($biodataMahasiswa->mulai_smt))->convertToSingle();

        return view('pdf.krs', [
                'krs' => $allKrsFromMahasiswa->getItems(),
                'mhs' => $biodataMahasiswa]
        );
    }

    /**
     * @param KrsRequest $krsRequest
     * @param $mahasiswa_id
     * @return \Dingo\Api\Http\Response|void
     */
    public function krsStore(KrsRequest $krsRequest, $mahasiswa_id)
    {

        $job = new CreateNilaiFromMahasiswa($this->mahasiswaRepo->show($mahasiswa_id), $krsRequest->all());

        if ($nilaiJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($nilaiJob, function($nilai){
                return[
                    'id' => $nilai->id_nilai
                ];
            });
        }

        return $this->response->error('Krs gagal disimpan', 422);

    }

    /**
     * @param $mahasiswa_id
     * @param $krs_id
     * @return \Illuminate\Http\Response
     */
    public function krsShow($mahasiswa_id, $krs_id)
    {
        $krsMahasiswa = $this->mahasiswaRepo->showKrs($mahasiswa_id, $krs_id);

        return $this->responseWebixItem($krsMahasiswa);
    }

    /**
     * @param $mahasiswa_id
     * @param $krs_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function krsDestroy($mahasiswa_id, $krs_id)
    {
        $job = new DestroyNilaiFromMahasiswa($this->mahasiswaRepo->show($mahasiswa_id), $krs_id);

        if ($nilaiJob = $this->dispatch($job)) {
            return $this->responseWebixDelete($nilaiJob);
        }

        return $this->response->error('Gagal menghapus nilai', 422);
    }

    public function krsDitawarkanIndex($mahasiswa_id)
    {
        $krsDitawarkan = $this->mahasiswaRepo->getKrsDitawarkan($mahasiswa_id);

        return $this->responseWebixCollection($krsDitawarkan, function ($data) {
            $dataArray = $data->toArray();
            $dataArray['smt'] = (new SemesterFactory($data->id_smt))->convertToSingle();

            return $dataArray;
        });
    }

    public function krsHerIndex($mahasiswa_id)
    {
        $krsHer = $this->mahasiswaRepo->getKrsHer($mahasiswa_id);

        return $this->responseWebixCollection($krsHer);
    }

    public function getKrsDiambil($mahasiswa_id)
    {
        $krsDiambil = $this->mahasiswaRepo->getKrsDiambil($mahasiswa_id);

        return $this->responseWebixCollection($krsDiambil, function ($data) {
            $dataArray = $data->toArray();
            $dataArray['id'] = $data->id_nilai;

            return $dataArray;
        });
    }
}
