<?php

namespace FE_UNSIQ\Http\Controllers\Kurikulum;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Http\Requests\KurikulumRequest;
use FE_UNSIQ\Jobs\Kurikulum\CreateSyncKurikulum;
use FE_UNSIQ\Jobs\Kurikulum\UpdateKurikulum;
use FE_UNSIQ\Repositories\Kurikulum\KurikulumRepository;
use Illuminate\Http\Request;

class KurikulumController extends Controller
{

    /**
     * @var KurikulumRepository
     */
    protected $kurikulumRepo;

    /**
     * KurikulumController constructor.
     * @param KurikulumRepository $kurikulumRepo
     */
    public function __construct(KurikulumRepository $kurikulumRepo)
    {
        $this->kurikulumRepo = $kurikulumRepo;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function kurikulumIndex()
    {
        $allKurikulum = $this->kurikulumRepo->getAll();

        return $this->responseWebixCollection($allKurikulum, function($kurikulum) {
            $kurikulumArray = $kurikulum->toArray();
            $kurikulumArray['id'] = $kurikulum->id_kurikulum_sp;

            return $kurikulumArray;
        });
    }

    /**
     * @return \Dingo\Api\Http\Response|void
     */
    public function kurikulumStore(KurikulumRequest $kurikulumRequest)
    {
        // execute some job
        $job = new CreateSyncKurikulum($kurikulumRequest->all());

        // return the response
        if ($kurikulumJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($kurikulumJob);
        }

        return $this->response->error('Terjadi kesalahan', 500);
    }

    /**
     * @param $kurikulum_id
     * @return \Illuminate\Http\Response
     */
    public function kurikulumShow($kurikulum_id)
    {
        $kurikulum = $this->kurikulumRepo->show($kurikulum_id);

        return $this->responseWebixItem($kurikulum);
    }

    /**
     * @param $kurikulum_id
     * @return \Illuminate\Http\Response|void
     */
    public function kurikulumUpdate(KurikulumRequest $kurikulumRequest, $kurikulum_id)
    {
        $job = new UpdateKurikulum($kurikulum_id, $kurikulumRequest->all());

        if ($kurikulumJob = $this->dispatch($job)) {
            return $this->responseWebixUpdate($kurikulumJob);
        }

        return $this->response->error('Gagal mengupdate kurikulum', 422);
    }

}