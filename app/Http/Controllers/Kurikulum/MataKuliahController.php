<?php

namespace FE_UNSIQ\Http\Controllers\Kurikulum;

use FE_UNSIQ\Http\Requests\MataKuliahKurikulumRequest;
use FE_UNSIQ\Jobs\Kurikulum\CreateMataKuliahFromKurikulum;
use FE_UNSIQ\Jobs\Kurikulum\DestroyMataKuliahFromKurikulum;
use FE_UNSIQ\Jobs\Kurikulum\UpdateMataKuliahFromKurikulum;

class MataKuliahController extends KurikulumController
{

    /**
     * @param $kurikulum_id
     * @return \Illuminate\Http\Response
     */
    public function mataKuliahIndex($kurikulum_id)
    {
        $allMataKuliahFromKurikulum = $this->kurikulumRepo->getMataKuliah($kurikulum_id);

        return $this->responseWebixCollection($allMataKuliahFromKurikulum, function ($data) {
            $makulKurikulum = $data->toArray();
            $makulKurikulum['id'] = $data->id_mk_kurikulum;

            return $makulKurikulum;
        });
    }

    /**
     * insert matakuliah pada kurikulum semester
     * @param $kurikulum_id
     * @return \Dingo\Api\Http\Response|void
     */
    public function mataKuliahstore(MataKuliahKurikulumRequest $kuliahKurikulumRequest, $kurikulum_id)
    {
        //FIXME: createsync
        $job = new CreateMataKuliahFromKurikulum($kurikulum_id, $kuliahKurikulumRequest->all());

        if ($mkkJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($mkkJob, function ($mkk){
                return [
                    'id' => $mkk->id_mk_kurikulum,
                    'smt'=> $mkk->smt,
                    'a_wajib' => $mkk->a_wajib
                ];
            });
        }

        return $this->response->error('Gagal membuat mata kuliah', 422);
    }

    /**
     * @param $kurikulum_id
     * @param $mata_kuliah_id
     * @return \Illuminate\Http\Response
     */
    public function mataKuliahShow($kurikulum_id, $mata_kuliah_id)
    {
        $mata_kuliah = $this->kurikulumRepo->showMataKuliahFromKurikulum($kurikulum_id, $mata_kuliah_id);

        return $this->responseWebixItem($mata_kuliah);
    }

    /**
     * update matakuliah kurikulum
     * @param $kurikulum_id
     * @param $mata_kuliah_id
     * @return \Illuminate\Http\Response|void
     */
    public function mataKuliahUpdate(MataKuliahKurikulumRequest $mataKuliahKurikulumRequest, $kurikulum_id, $mata_kuliah_id)
    {
        $job = new UpdateMataKuliahFromKurikulum($mata_kuliah_id, $mataKuliahKurikulumRequest->all());

        if ($mkkJob = $this->dispatch($job)) {
            return $this->responseWebixUpdate($mkkJob);
        }

        return $this->response->error('Gagal mengupdate mata kuliah', 422);
    }

    /**
     * @param $kurikulum_id
     * @param $mata_kuliah_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function mataKuliahDestroy($kurikulum_id, $mata_kuliah_id)
    {
        $job = new DestroyMataKuliahFromKurikulum($kurikulum_id, $mata_kuliah_id);

        if ($mkkJob = $this->dispatch($job)) {
            return $this->responseWebixDelete($mkkJob);
        }

        return $this->response->error('Gagal menghapus mata kuliah', 422);
    }

}