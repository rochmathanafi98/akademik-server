<?php

namespace FE_UNSIQ\Http\Controllers\KelasKuliah;

use FE_UNSIQ\Http\Requests\NilaiRequest;
use FE_UNSIQ\Jobs\Nilai\UpdateNilaiFromKelasKuliah;
use FE_UNSIQ\Services\Semester\SemesterFactory;

class NilaiController extends KelasKuliahController
{

    /**
     * @param $kelas_kuliah_id
     * @return \Illuminate\Http\Response
     */
    public function nilaiIndex($kelas_kuliah_id)
    {
        $nilaiResource = $this->kelasKuliahRepo->getNilai($kelas_kuliah_id);
        $nilaiResponse = $this->responseWebixCollection($nilaiResource, function($nilai) {
            return [
                'id' => $nilai->id_nilai,
                'smt_mhs' => (new SemesterFactory())->setTahunSmt($nilai->mulai_smt)->convertToSingle(),
                'nm_kls' => $nilai->nm_kls,
                'nipd' => $nilai->nipd,
                'nm_pd' => $nilai->nm_pd,
                'mulai_smt' => $nilai->mulai_smt,
                'nilai_angka' => $nilai->nilai_angka,
                'nilai_huruf' => $nilai->nilai_huruf,
                'nilai_indeks' => $nilai->nilai_indeks,
                'id_sms' => $nilai->id_sms
            ];
        });

        return $nilaiResponse;
    }

    /**
     * @param $kelas_kuliah_id
     * @param $nilai_id
     * @return \Illuminate\Http\Response
     */
    public function nilaiShow($kelas_kuliah_id, $nilai_id)
    {
        $nilaiItem = $this->kelasKuliahRepo->showNilaiFromKelasKuliah($kelas_kuliah_id, $nilai_id);

        return $this->responseWebixItem($nilaiItem);
    }

    /**
     * @param $kelas_kuliah_id
     * @param $nilai_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function nilaiUpdate(NilaiRequest $nilaiRequest, $kelas_kuliah_id, $nilai_id)
    {
        $job = new UpdateNilaiFromKelasKuliah($kelas_kuliah_id, $nilai_id, $nilaiRequest->all());

        if ($nilaiJob = $this->dispatch($job)) {
            return $this->responseWebixUpdate($nilaiJob, function ($nilai){
                return [
                    'id' => $nilai->id_nilai,
                    'nilai_angka' => $nilai->nilai_angka,
                    'nilai_indeks' => $nilai->nilai_indeks,
                    'nilai_huruf' => $nilai->nilai_huruf,
                ];
            });
        }

        return $this->response->error('Gagal mengupdate nilai', 422);
    }

}