<?php

namespace FE_UNSIQ\Http\Controllers\KelasKuliah;

use FE_UNSIQ\Http\Requests\AjarDosenRequest;
use FE_UNSIQ\Jobs\Dosen\DeleteAjarDosen;
use FE_UNSIQ\Jobs\KelasKuliah\CreateAjarDosen;
use Illuminate\Http\Request;

class DosenController extends KelasKuliahController
{

    /**
     * ajar dosen dari kelaskuliah
     * @param $kelas_kuliah_id
     * @return \Illuminate\Http\Response
     */
    public function dosenIndex($kelas_kuliah_id)
    {
        $dosenResource = $this->kelasKuliahRepo->getDosen($kelas_kuliah_id);
        $dosenResponse = $this->responseWebixCollection($dosenResource, function($dosen) {
            return [
                'id'            => $dosen->id_ajar,
                'nidn'          => $dosen->nidn,
                'nm_ptk'        => $dosen->nm_ptk,
                'nm_subst'      => $dosen->nm_subst,
                'jml_tm_renc'   => $dosen->jml_tm_renc,
                'jml_tm_real'   => $dosen->jml_tm_real
            ];
        });

        return $dosenResponse;
    }

    public function dosenStore(AjarDosenRequest $ajarDosenRequest)
    {
        $job = new CreateAjarDosen($ajarDosenRequest->all());

        if ($dosenJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($dosenJob);
        }

        return $this->response->error('Gagal menyimpan ajar dosen', 422);
    }

    public function dosenDestroy($id_kelas, $id_ajar_dosen)
    {
        unset($id_kelas);

        $job = new DeleteAjarDosen($id_ajar_dosen);

        if ($dosenJob = $this->dispatch($job)) {
            return $this->responseWebixDelete($dosenJob);
        }

        return $this->response->error('Gagal menghapus ajar dosen', 422);
    }

}