<?php

namespace FE_UNSIQ\Http\Controllers\KelasKuliah;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Http\Requests\KelasKuliahRequest;
use FE_UNSIQ\Repositories\KelasKuliah\KelasKuliahRepository;
use Illuminate\Http\Request;

class KelasKuliahController extends Controller
{

    /**
     * @var KelasKuliahRepository
     */
    protected $kelasKuliahRepo;

    /**
     * KelasKuliahController constructor.
     * @param KelasKuliahRepository $kelasKuliahRepo
     */
    public function __construct(KelasKuliahRepository $kelasKuliahRepo)
    {
        $this->kelasKuliahRepo = $kelasKuliahRepo;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function kelasKuliahIndex()
    {
        $kelasKuliahResource = $this->kelasKuliahRepo->getAll();
        $kelasKuliahResponse = $this->responseWebixCollection($kelasKuliahResource, function ($kelas_kuliah) {
            return [
                'id' => $kelas_kuliah->id_kls,
                'id_mk' => $kelas_kuliah->id_mk,
                'id_sms' => $kelas_kuliah->id_sms,
                'smt' => $kelas_kuliah->smt,
                'id_kls' => $kelas_kuliah->id_kls,
                'id_smt' => $kelas_kuliah->id_smt,
                'nm_kls' => $kelas_kuliah->nm_kls,
                'kode_mk' => $kelas_kuliah->kode_mk,
                'nm_mk' => $kelas_kuliah->nm_mk,
                'sks_mk' => $kelas_kuliah->sks_mk,
            ];
        });

        return $kelasKuliahResponse;
    }

}
