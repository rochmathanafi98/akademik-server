<?php

namespace FE_UNSIQ\Http\Controllers\KelasKuliah;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Http\Requests\KrsRequest;
use FE_UNSIQ\Jobs\Nilai\CreateNilaiFromKelasKuliah;
use FE_UNSIQ\Jobs\Nilai\DestroyNilaiFromKelasKuliah;

class MahasiswaController extends KelasKuliahController
{

    /**
     * @param $kelas_kuliah_id
     * @return \Illuminate\Http\Response
     */
    public function mahasiswaIndex($kelas_kuliah_id)
    {
        $mahasiswaResource = $this->kelasKuliahRepo->getMahasiswa($kelas_kuliah_id);
        $mahasiswaResponse = $this->responseWebixCollection($mahasiswaResource, function ($mhs) {
            return [
                'id' => $mhs->id_nilai,
                'nipd' => $mhs->nipd,
                'nm_pd' => $mhs->nm_pd,
                'jk' => $mhs->jk,
                'nm_prodi' => $mhs->nm_lemb
            ];
        });

        return $mahasiswaResponse;
    }


    /**
     * memasukkan mahasiswa dalam kelas (krs)
     * @param KrsRequest $krsRequest
     * @param $mahasiswa_id
     * @return mixed|void
     */
    public function mahasiswaStore(KrsRequest $krsRequest, $kelas_kuliah_id)
    {
        $krsData = $krsRequest->all();

        $kelasKuliah = KelasKuliah::findOrFail($kelas_kuliah_id);
        $krsData = array_merge($krsData, $kelasKuliah->toArray());
        $job = new CreateNilaiFromKelasKuliah($kelasKuliah, $krsData);

        if ($nilaiJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($nilaiJob, function ($nilai) {
                return [
                    'id' => $nilai->id_nilai
                ];
            });
        }

        return $this->response->error('Krs gagal disimpan', 422);

    }

    /**
     * destroy krs mahasiswa dari kelaskuliah
     * @param $kelas_kuliah_id
     * @param $nilai_id
     * @return mixed|void
     */
    public function mahasiswaDestroy($kelas_kuliah_id, $nilai_id)
    {
        $job = new DestroyNilaiFromKelasKuliah(KelasKuliah::findOrFail($kelas_kuliah_id), $nilai_id);

        if ($nilaiJob = $this->dispatch($job)) {
            return $this->responseWebixDelete($nilaiJob);
        }

        return $this->response->error('Gagal menghapus krs', 422);
    }

    public function absenIsiCetak($kelas_kuliah_id)
    {
        $allMahasiswaKrs = $this->kelasKuliahRepo->getMahasiswa($kelas_kuliah_id);
        $dosen = $this->kelasKuliahRepo->getOneDosen($kelas_kuliah_id);
        $mata_kuliah = $this->kelasKuliahRepo->showKelasKuliahDetail($kelas_kuliah_id);

        return view('pdf.isiabsen', [
            'krs' => $allMahasiswaKrs->getItems(),
            'dosen' => $dosen,
            'kelas' => $mata_kuliah
        ]);
    }

    public function absenCoverCetak($kelas_kuliah_id)
    {
        $allMahasiswaKrs = $this->kelasKuliahRepo->getMahasiswa($kelas_kuliah_id);
        $dosen = $this->kelasKuliahRepo->getOneDosen($kelas_kuliah_id);
        $mata_kuliah = $this->kelasKuliahRepo->showKelasKuliahDetail($kelas_kuliah_id);

        return view('pdf.coverabsen', [
            'krs' => $allMahasiswaKrs->getItems(),
            'dosen' => $dosen,
            'kelas' => $mata_kuliah
        ]);
    }

}