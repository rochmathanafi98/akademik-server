<?php

namespace FE_UNSIQ\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use FE_UNSIQ\Services\Webix\Response\ResponseHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Helpers, ResponseHelper;

    /**
     * @param $validationRules
     */
    public function validateRequest($validationRules)
    {
        // validate the request
        try {
            $this->validate($this->request, $validationRules);
        } catch (HttpResponseException $e) {

            // get the errors message from session response
            $validationErrors = $e->getResponse()->getSession()->get('errors');
            $errors = [];
            foreach ($validationErrors->getBag('default')->getMessages() as $message) {
                $errors[] = $message;
            }

            return $this->response->error(json_encode($errors), 422);
        }
    }
}
