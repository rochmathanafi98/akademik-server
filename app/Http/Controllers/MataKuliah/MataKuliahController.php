<?php

namespace FE_UNSIQ\Http\Controllers\MataKuliah;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Http\Requests\MataKuliahRequest;
use FE_UNSIQ\Jobs\MataKuliah\CreateSyncMataKuliah;
use FE_UNSIQ\Jobs\MataKuliah\UpdateMataKuliah;
use FE_UNSIQ\Repositories\MataKuliah\MataKuliahRepository;
use Illuminate\Http\Request;

class MataKuliahController extends Controller
{

    /**
     * @var
     */
    protected $mataKuliahRepo;

    /**
     * @var MataKuliahRepository
     */
    private $mataKuilahRepo;

    /**
     * MataKuliahController constructor.
     * @param MataKuliahRepository $mataKuilahRepo
     */
    public function __construct(MataKuliahRepository $mataKuilahRepo)
    {
        $this->mataKuilahRepo = $mataKuilahRepo;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function mataKuliahIndex()
    {
        $mataKuliahResource = $this->mataKuilahRepo->getAll();
        $mataKuliahResponse = $this->responseWebixCollection($mataKuliahResource, function($mk) {
            return [
                'id' => $mk->id_mk,
                'kode_mk' => $mk->kode_mk,
                'nm_mk' => $mk->nm_mk,
                'nm_prodi' => $mk->nm_lemb,
                'nm_jns_mk' => $mk->jns_mk,
                'nm_kel_mk' => $mk->kel_mk,
                'sks_mk' =>$mk->sks_mk,
                'sks_tm' => $mk->sks_tm,
                'sks_prak' => $mk->sks_prak,
                'sks_prak_lap' => $mk->sks_prak_lap,
                'sks_sim' => $mk->sks_sim,
                'a_bahan_ajar' => $mk->a_bahan_ajar,
                'a_sap' => $mk->a_sap,
                'a_silabus' => $mk->a_silabus,
            ];
        });

        return $mataKuliahResponse;
    }

    public function mataKuliahStore(MataKuliahRequest $mataKuliahRequest)
    {
        // execute the jobs
        $mkData = $mataKuliahRequest->all();
        $mkData['id_jenj_didik'] = (int) $mkData['id_jenj_didik'];
        $mkData['sks_mk'] = (int) $mkData['sks_mk'];
        $mkData['a_sap'] = (int) $mkData['a_sap'];
        $mkData['a_silabus'] = (int) $mkData['a_silabus'];
        $mkData['a_bahan_ajar'] = (int) $mkData['a_bahan_ajar'];
        $mkData['acara_prak'] = (int) $mkData['acara_prak'];
        $mkData['a_diktat'] = (int) $mkData['a_diktat'];
        unset($mkData['webix_operation']);

//        dd($mkData);

        $job = new CreateSyncMataKuliah($mkData);

        if ($mkJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($mkJob, function($mk){
                return [
                    'id' => $mk->id_mk
                ];
            });
        }

        return $this->response->error('Terjadi kesalahan', 500);
    }

    /**
     * @param $mata_kuliah_id
     * @return \Illuminate\Http\Response
     */
    public function mataKuliahShow($mata_kuliah_id)
    {
        $mataKuliah = $this->mataKuilahRepo->show($mata_kuliah_id);

        return $this->responseWebixItem($mataKuliah);
    }

    /**
     * @param $mata_kuliah_id
     * @return \Illuminate\Http\Response|void
     */
    public function mataKuliahUpdate(MataKuliahRequest $mataKuliahRequest, $mata_kuliah_id)
    {
        $job = new UpdateMataKuliah($mata_kuliah_id, $mataKuliahRequest->all());

        if ($this->dispatch($job)) {
            return $this->mataKuliahShow($mata_kuliah_id);
        }

        return $this->response->error('Gagal mengupdate mata kuliah', 422);
    }
}