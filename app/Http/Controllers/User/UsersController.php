<?php

namespace FE_UNSIQ\Http\Controllers\User;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Repositories\User\UserRepository;

class UsersController extends Controller
{
    /**
     * @param UserRepository $userRepository
     * @return \Illuminate\Http\Response
     */
    public function index(UserRepository $userRepository)
    {
        return $this->responseWebixCollection($userRepository->getAll());
    }

    /**
     * @param UserRepository $userRepository
     * @param $user_id
     * @return \Illuminate\Http\Response
     */
    public function show(UserRepository $userRepository, $user_id)
    {
        return $this->responseWebixItem($userRepository->show($user_id));
    }
}