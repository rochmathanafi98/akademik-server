<?php

namespace FE_UNSIQ\Http\Controllers\Semester;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Repositories\Semester\SemesterRepository;
use Illuminate\Http\Request;

class SemesterController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var SemesterRepository
     */
    protected $semesterRepo;

    /**
     * SemesterController constructor.
     * @param Request $request
     * @param SemesterRepository $semesterRepo
     */
    public function __construct(Request $request, SemesterRepository $semesterRepo)
    {
        $this->request = $request;
        $this->semesterRepo = $semesterRepo;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function semesterIndex()
    {
        $semesterResource = $this->semesterRepo->getAll();
        $semesterResponse = $this->responseWebixCollection($semesterResource, function($semester) {
            $semesterArray = $semester->toArray();
            $semesterArray['id'] = $semester->id_smt;

            return $semesterArray;
        });

        return $semesterResponse;
    }
}