<?php

namespace FE_UNSIQ\Http\Controllers\Dashboard\Semester;

use FE_UNSIQ\Http\Controllers\Dashboard\DashboardController;

class MahasiswaController extends DashboardController
{
    
    /**
     * @param $semester_id
     * @return \Illuminate\Http\Response
     */
    public function getCountMahasiswaFromSemester($semester_id)
    {
        $jumlahMhsKeluar = $this->dashboardRepo->getCountsMahasiswaFromSemester($semester_id);

        return $this->responseWebixItem($jumlahMhsKeluar);
    }

}