<?php

namespace FE_UNSIQ\Http\Controllers\Prodi;

class MataKuliahController extends ProdiController
{

    /**
     * @param $prodi_id
     * @return \Illuminate\Http\Response
     */
    public function mataKuliahIndex($prodi_id)
    {
        $mataKuliahResource = $this->prodiRepo->getMataKuliah($prodi_id);

        return $this->responseWebixCollection($mataKuliahResource);
    }

    /**
     * @param $prodi_id
     * @param $mata_kuliah_id
     * @return \Illuminate\Http\Response
     */
    public function mataKuliahShow($prodi_id, $mata_kuliah_id)
    {
        $mataKuliahItem = $this->prodiRepo->showMataKuliahFromProdi($prodi_id, $mata_kuliah_id);

        return $this->responseWebixItem($mataKuliahItem);
    }

}