<?php

namespace FE_UNSIQ\Http\Controllers\Prodi;

use FE_UNSIQ\Jobs\Mahasiswa\DestroyMahasiswaKeluarFromProdi;
use FE_UNSIQ\Jobs\Mahasiswa\StoreMahasiswaKeluarFromProdi;
use FE_UNSIQ\Jobs\Mahasiswa\UpdateMahasiswaKeluarFromProdi;

class MahasiswaKeluarController extends ProdiController
{

    /**
     * @param $prodi_id
     * @return \Illuminate\Http\Response
     */
    public function mahasiswaKeluarIndex($prodi_id)
    {
        $mahasiswaKeluarResource = $this->prodiRepo->getMahasiswaKeluar($prodi_id);
        $mahasiswaKeluarResponse = $this->responseWebixCollection($mahasiswaKeluarResource, function($mahasiswa) {
            return [
                'id' => $mahasiswa->id_reg_pd,
                'nipd' => $mahasiswa->nipd,
                'nm_pd' => $mahasiswa->nm_pd,
                'mulai_smt' => $mahasiswa->mulai_smt,
                'nm_jenis_keluar' => $mahasiswa->ket_keluar,
                'ket' => $mahasiswa->ket_keluar,
                'tgl_keluar' => $mahasiswa->tgl_keluar,
            ];
        });

        return $mahasiswaKeluarResponse;
    }

    /**
     * @param $prodi_id
     * @param $mahasiswa_keluar_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function mahasiswaKeluarStore($prodi_id, $mahasiswa_keluar_id)
    {
        $job = new StoreMahasiswaKeluarFromProdi($prodi_id, $mahasiswa_keluar_id, $this->request->all());

        if ($this->dispatch($job)) {
            return $this->mahasiswaKeluarShow($prodi_id, $mahasiswa_keluar_id)->setStatusCode(201);
        }

        return $this->response->error('Gagal mengupdate mahasiswa', 422);
    }

    /**
     * @param $prodi_id
     * @param $mahasiswa_keluar_id
     * @return \Illuminate\Http\Response
     */
    public function mahasiswaKeluarShow($prodi_id, $mahasiswa_keluar_id)
    {
        $mahasiswaKeluarItem = $this->prodiRepo->showMahasiswaKeluarFromProdi($prodi_id, $mahasiswa_keluar_id);

        return $this->responseWebixItem($mahasiswaKeluarItem);
    }

    /**
     * @param $prodi_id
     * @param $mahasiswa_keluar_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function mahasiswaKeluarUpdate($prodi_id, $mahasiswa_keluar_id)
    {
        $job = new UpdateMahasiswaKeluarFromProdi($prodi_id, $mahasiswa_keluar_id, $this->request->all());

        if ($this->dispatch($job)) {
            return $this->mahasiswaKeluarShow($prodi_id, $mahasiswa_keluar_id)->setStatusCode(202);
        }

        return $this->response->error('Gagal mengupdate mahasiswa', 422);
    }

    /**
     * @param $prodi_id
     * @param $mahasiswa_keluar_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function mahasiswakeluarDestroy($prodi_id, $mahasiswa_keluar_id)
    {
        $job = new DestroyMahasiswaKeluarFromProdi($prodi_id, $mahasiswa_keluar_id, $this->request->all());

        if ($this->dispatch($job)) {
            return $this->response->noContent()->setStatusCode(200);
        }

        return $this->response->error('Gagal menghapus mahasiswa keluar', 422);
    }

}