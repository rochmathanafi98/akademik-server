<?php

namespace FE_UNSIQ\Http\Controllers;

use FE_UNSIQ\Jobs\KelasKuliah\CreateKelasKuliahFromSemester;

class TestController extends Controller
{
    public function debug($data)
    {
        $semester_id = '20151';

        dd($this->dispatch(new CreateKelasKuliahFromSemester($semester_id, $data)));
    }
}