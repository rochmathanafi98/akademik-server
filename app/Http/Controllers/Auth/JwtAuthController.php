<?php

namespace FE_UNSIQ\Http\Controllers\Auth;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Services\Auth\ApiAuthenticator;
use FE_UNSIQ\Services\Auth\ApiAuthenticatorException;
use Illuminate\Http\Request;

class JwtAuthController extends Controller
{

    /**
     * @var ApiAuthenticator
     */
    protected $apiAuthenticator;

    /**
     * JwtAuthController constructor.
     * @param ApiAuthenticator $apiAuthenticator
     */
    public function __construct(ApiAuthenticator $apiAuthenticator)
    {
        $this->apiAuthenticator = $apiAuthenticator;
    }

    /**
     * @param Request $request
     * @return string|void
     */
    public function generateToken(Request $request)
    {
        try {
            $token = $this->apiAuthenticator->authenticate($request->only('username', 'password'));
            return response()->json(['jwt_token' => $token]);
        } catch (ApiAuthenticatorException $e) {
            return $this->response->error($e->getMessage(), $e->getStatusCode());
        }
    }
    
}