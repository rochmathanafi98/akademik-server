<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

class RiwayatKepangkatanController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function riwayatKepangkatanIndex($dosen_id)
    {
        $allRiwayatKepangkatan = $this->dosenRepo->getRiwayatKepangkatan($dosen_id);
        
        return $this->responseWebixCollection($allRiwayatKepangkatan);
    }

}