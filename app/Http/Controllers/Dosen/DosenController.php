<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

use FE_UNSIQ\Http\Controllers\Controller;
use FE_UNSIQ\Http\Requests\DosenRequest;
use FE_UNSIQ\Jobs\Dosen\UpdateDosen;
use FE_UNSIQ\Repositories\Dosen\DosenRepository;

class DosenController extends Controller
{

    /**
     * @var DosenRepository
     */
    protected $dosenRepo;

    /**
     * DosenController constructor.
     * @param DosenRepository $dosenRepo
     */
    public function __construct(DosenRepository $dosenRepo)
    {
        $this->dosenRepo = $dosenRepo;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function dosenIndex()
    {
        $dosenResource = $this->dosenRepo->getAllDosen();
        $dosenResponse = $this->responseWebixCollection($dosenResource, function ($dosen) {

            return [
                'id' => $dosen->id_reg_ptk,
                'id_reg_ptk' => $dosen->id_reg_ptk,
                'id_ptk' => $dosen->id_ptk,
                'nidn' => $dosen->nidn,
                'nm_ptk' => $dosen->nm_ptk,
                'jk' => $dosen->jk,
                'tmpt_lahir' => $dosen->tmpt_lahir,
                'nm_agama' => $dosen->nm_agama,
                'nm_stat_pegawai' => $dosen->nm_stat_pegawai,
                'nm_ikatan_kerja' =>$dosen->nm_ikatan_kerja,
                'nm_stat_aktif' =>$dosen->nm_stat_aktif,
                'nm_prodi' =>$dosen->nm_lemb,
                'thn_ajaran' =>$dosen->id_thn_ajaran
            ];
        });

        return $dosenResponse;
    }

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function dosenShow($dosen_id)
    {
        $dosen = $this->dosenRepo->show($dosen_id);

        return $this->responseWebixItem($dosen);
    }

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response|void
     */
    public function dosenUpdate(DosenRequest $dosenRequest, $dosen_id)
    {
        $job = new UpdateDosen($dosen_id, $dosenRequest->all());

        if ($this->dispatch($job)) {
            return $this->dosenShow($dosen_id);
        }

        return $this->response->error('Gagal mengupdate dosen', 422);
    }

}