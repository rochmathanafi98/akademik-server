<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

class RiwayatPendidikanController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function riwayatPendidikanIndex($dosen_id)
    {
        $allRiwayatPendidikan = $this->dosenRepo->getRiwayatPendidikan($dosen_id);

        return $this->responseWebixCollection($allRiwayatPendidikan);
    }

}