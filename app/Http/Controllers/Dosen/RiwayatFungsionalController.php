<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

class RiwayatFungsionalController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function riwayatFungsionalIndex($dosen_id)
    {
        $allRiwayatFungsionalDosen = $this->dosenRepo->getRiwayatFungsional($dosen_id);

        return $this->responseWebixCollection($allRiwayatFungsionalDosen );
    }

}