<?php

namespace FE_UNSIQ\Http\Controllers\Dosen;

use FE_UNSIQ\Http\Requests\AjarDosenRequest;
use FE_UNSIQ\Jobs\Dosen\CreateAjarDosen;
use FE_UNSIQ\Jobs\Dosen\DeleteAjarDosen;

class AjarDosenController extends DosenController
{

    /**
     * @param $dosen_id
     * @return \Illuminate\Http\Response
     */
    public function ajarDosenIndex($dosen_id)
    {
        $allAjarDosen = $this->dosenRepo->getAjarDosen($dosen_id);

        return $this->responseWebixCollection($allAjarDosen, function ($dosen) {
            return [
                "id" => $dosen->id_ajar,
                "id_smt" => $dosen->id_smt,
                "id_reg_ptk" => $dosen->id_reg_ptk,
                "id_subst" => $dosen->id_subst,
                "id_kls" => $dosen->id_kls,
                "jml_tm_renc" => $dosen->jml_tm_renc,
                "jml_tm_real" => $dosen->jml_tm_real,
                "id_jns_eval" => $dosen->id_jns_eval,
                "id_sms" => $dosen->id_sms,
                "id_mk" => $dosen->id_mk,
                "nm_kls" => $dosen->nm_kls,
                "sks_mk" => $dosen->sks_mk,
                "sks_tm" => $dosen->sks_tm,
                "sks_prak" => $dosen->sks_prak,
                "sks_prak_lap" => $dosen->sks_prak_lap,
                "sks_sim" => $dosen->sks_sim,
                "bahasan_case" => $dosen->bahasan_case,
                "tgl_mulai_koas" => $dosen->tgl_mulai_koas,
                "tgl_selesai_koas" => $dosen->tgl_selesai_koas,
                "kode_mk" => $dosen->kode_mk,
                "nm_mk" => $dosen->nm_mk,
                "jns_mk" => $dosen->jns_mk,
                "kel_mk" => $dosen->kel_mk,
                "metode_pelaksanaan_kuliah" => $dosen->metode_pelaksanaan_kuliah,
                "a_sap" => $dosen->a_sap,
                "a_silabus" => $dosen->a_silabus,
                "a_bahan_ajar" => $dosen->a_bahan_ajar,
                "acara_prak" => $dosen->acara_prak,
                "a_diktat" => $dosen->a_diktat,
                "tgl_mulai_efektif" => $dosen->tgl_mulai_efektif,
                "tgl_akhir_efektif" => $dosen->tgl_akhir_efektif,
                "id_mk_kurikulum" => $dosen->id_mk_kurikulum,
                "id_kurikulum_sp" => $dosen->id_kurikulum_sp,
                "smt" => $dosen->smt,
                "a_wajib" => $dosen->a_wajib,
            ];
        });
    }

    /**
     * @param $dosen_id
     * @return \Dingo\Api\Http\Response|void
     */
    public function ajarDosenStore(AjarDosenRequest $ajarDosenRequest, $dosen_id)
    {
        $job = new CreateAjarDosen($dosen_id, $ajarDosenRequest->all());

        if ($dosenJob = $this->dispatch($job)) {
            return $this->responseWebixCreate($dosenJob);
        }

        return $this->response->error('Gagal menyimpan ajar dosen', 422);
    }

    /**
     * @param $dosen_id
     * @param $id_ajar_dosen
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function ajarDosenDestroy($dosen_id, $id_ajar_dosen)
    {
        unset($dosen_id);

        $job = new DeleteAjarDosen($id_ajar_dosen);

        if ($dosenJob = $this->dispatch($job)) {
            return $this->responseWebixDelete($dosenJob);
        }

        return $this->response->error('Gagal menghapus ajar dosen', 422);
    }
}