<?php

namespace FE_UNSIQ\Repositories\KelasKuliah;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Eloquent\AjarDosen;
use FE_UNSIQ\Repositories\Repository;

class KelasKuliahRepository extends Repository
{
    /**
     * @var KelasKuliah
     */
    protected $kelasKuliah;

    /**
     * KelasKuliahRepository constructor.
     * @param KelasKuliah $kelasKuliah
     */
    public function __construct(KelasKuliah $kelasKuliah)
    {
        $this->kelasKuliah = $kelasKuliah;
    }

    /**
     * @return Resource
     */
    public function getAll()
    {
        $kelasKuliahBuilder = $this->kelasKuliah->select()
            ->join('mata_kuliah', 'kelas_kuliah.id_mk', '=', 'mata_kuliah.id_mk')
            ->join('mata_kuliah_kurikulum', 'mata_kuliah.id_mk', '=', 'mata_kuliah_kurikulum.id_mk')
            ->join('sms', 'mata_kuliah.id_sms', '=', 'sms.id_sms')
            ->orderBy('id_smt', 'desc')
            ->orderBy('smt', 'asc')
            ->orderBy('nm_kls', 'asc')
            ->orderBy('nm_kls', 'asc')
            ->orderBy('kode_mk', 'asc');

        $kelasKuliahResource = $this->makeDynamicResource($kelasKuliahBuilder);

        return $kelasKuliahResource;
    }

    public function showKelasKuliahDetail($kelas_kuliah_id)
    {
        $kelas = $this->kelasKuliah->findOrFail($kelas_kuliah_id);

        $kelasKuliahBuilder = $kelas->mata_kuliah()
            ->join('mata_kuliah_kurikulum', 'mata_kuliah.id_mk', '=', 'mata_kuliah_kurikulum.id_mk')
            ->join('sms', 'mata_kuliah.id_sms', '=', 'sms.id_sms')
            ->firstOrFail();
        return $kelasKuliahBuilder;

    }

    /**
     * @param $kelas_kuliah_id
     * @return Resource
     */
    public function getMahasiswa($kelas_kuliah_id)
    {
        $mahasiswaBuilder = $this->kelasKuliah
            ->findOrFail($kelas_kuliah_id)
            ->nilai()
            ->select('mahasiswa.*', 'mahasiswa_pt.*', 'sms.nm_lemb', 'nilai.*')
            ->join('mahasiswa_pt', 'nilai.id_reg_pd', '=', 'mahasiswa_pt.id_reg_pd')
            ->join('mahasiswa', 'mahasiswa_pt.id_pd', '=', 'mahasiswa.id_pd')
            ->join('sms', 'mahasiswa_pt.id_sms', '=', 'sms.id_sms')
            ->getQuery();

        $mahasiswaResource = $this->makeDynamicResource($mahasiswaBuilder);

        return $mahasiswaResource;
    }

    /**
     * @param $kelas_kuliah_id
     * @return Resource
     */
    public function getDosen($kelas_kuliah_id)
    {
        $dosenBuilder = $this->kelasKuliah->findOrFail($kelas_kuliah_id)->ajar_dosen()
            ->join('dosen_pt', 'dosen_pt.id_reg_ptk', '=', 'ajar_dosen.id_reg_ptk')
            ->join('dosen', 'dosen.id_ptk', '=', 'dosen_pt.id_ptk')
            ->getQuery();

        $dosenResource = $this->makeDynamicResource($dosenBuilder);

        return $dosenResource;
    }

    public function getOneDosen($kelas_kuliah_id)
    {
        $kelas = $this->kelasKuliah->findOrFail($kelas_kuliah_id);
        $dosenBuilder = $kelas->ajar_dosen()
            ->join('dosen_pt', 'dosen_pt.id_reg_ptk', '=', 'ajar_dosen.id_reg_ptk')
            ->join('dosen', 'dosen.id_ptk', '=', 'dosen_pt.id_ptk')
            ->first();

        return $dosenBuilder;

        return $dosen;

    }

    /**
     * @param $kelas_kuliah_id
     * @return Resource
     */
    public function getNilai($kelas_kuliah_id)
    {
        $nilaiBuilder = $this->kelasKuliah
            ->findOrFail($kelas_kuliah_id)
            ->nilai()
            ->join('mahasiswa_pt', 'nilai.id_reg_pd', '=', 'mahasiswa_pt.id_reg_pd')
            ->join('mahasiswa', 'mahasiswa_pt.id_pd', '=', 'mahasiswa.id_pd')
            ->join('kelas_kuliah', 'kelas_kuliah.id_kls', '=', 'nilai.id_kls')
            ->join('mata_kuliah', 'mata_kuliah.id_mk', '=', 'kelas_kuliah.id_mk')
            ->getQuery();

        $nilaiResource = $this->makeDynamicResource($nilaiBuilder);

        return $nilaiResource;
    }

    /**
     * @param $kelas_kuliah_id
     * @param $nilai_id
     * @return mixed
     */
    public function showNilaiFromKelasKuliah($kelas_kuliah_id, $nilai_id)
    {
        $nilaiItem = $this->kelasKuliah
            ->findOrFail($kelas_kuliah_id)
            ->nilai()
            ->join('mahasiswa_pt', 'nilai.id_reg_pd', '=', 'mahasiswa_pt.id_reg_pd')
            ->join('mahasiswa', 'mahasiswa_pt.id_pd', '=', 'mahasiswa.id_pd')
            ->getQuery()
            ->findOrFail($nilai_id);

        return $nilaiItem;
    }
}