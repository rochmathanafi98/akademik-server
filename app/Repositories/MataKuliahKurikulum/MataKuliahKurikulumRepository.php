<?php

namespace FE_UNSIQ\Repositories\MataKuliahKurikulum;

use FE_UNSIQ\Eloquent\MataKuliahKurikulum;
use FE_UNSIQ\Repositories\Repository;

class MataKuliahKurikulumRepository extends Repository
{
    public function getAll()
    {
        $makulBuilder = MataKuliahKurikulum::select()
            ->join('kurikulum', 'kurikulum.id_kurikulum_sp', '=', 'mata_kuliah_kurikulum.id_kurikulum_sp')
            ->join('mata_kuliah', 'mata_kuliah.id_mk', '=', 'mata_kuliah_kurikulum.id_mk')
            ->orderBy('nm_kurikulum_sp')
            ->orderBy('kode_mk');

        $makulKurikulumResource = $this->makeDynamicResource($makulBuilder);

        return $makulKurikulumResource;
    }
}