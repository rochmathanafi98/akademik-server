<?php

namespace FE_UNSIQ\Repositories;

use FE_UNSIQ\Services\Webix\DynamicLoad\DynamicLoadTrait;

abstract class Repository
{
    use DynamicLoadTrait;
}