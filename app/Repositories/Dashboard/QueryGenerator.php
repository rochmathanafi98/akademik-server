<?php
/**
 * Created by PhpStorm.
 * User: fahri
 * Date: 4/25/2016
 * Time: 2:13 AM
 */

namespace FE_UNSIQ\Repositories\Dashboard;


trait QueryGenerator
{

    /**
     * @param $fieldName
     * @param string $concat
     * @return string
     */
    protected function generateMahasiswaKeluarQuery($fieldName, $concat = '')
    {
        $sql = '
            SELECT
                COUNT(*) as ' . $fieldName . '
            FROM
                (
                    SELECT
                        *,
                        IF (get_thsmt (SUBSTR(tgl_keluar, 1, 4), MONTH (tgl_keluar)) = '. $this->semester_id .', TRUE, FALSE) 
                            AS keluar_tahun_ini
                    FROM
                        mahasiswa_pt AS mahasiswa_pt
                ) AS b 
                INNER JOIN mahasiswa 
                USING (id_pd)
        ';

        return $sql . $concat;
    }

    /**
     * @param $fieldName
     * @param string $concat
     * @return string
     */
    protected function generateMahasiswaRegistrasiQuery($fieldName, $concat = '')
    {
        $sql = '
            SELECT
                COUNT(*) AS ' . $fieldName . '
            FROM
                (
                    SELECT
                        *,
                    IF (
                        mulai_smt = ' . $this->semester_id . ',
                        TRUE,
                        FALSE
                    ) AS masuk_tahun_ini
                    FROM
                        mahasiswa_pt AS mahasiswa_pt
                ) AS b
            INNER JOIN mahasiswa USING (id_pd) WHERE b.masuk_tahun_ini = TRUE
        ';

        return $sql . $concat;
    }

}