<?php

namespace FE_UNSIQ\Repositories\Kurikulum;

use FE_UNSIQ\Eloquent\Kurikulum;
use FE_UNSIQ\Repositories\Repository;

class KurikulumRepository extends Repository
{

    /**
     * @var Kurikulum
     */
    protected $kurikulum;

    /**
     * KurikulumRepository constructor.
     * @param Kurikulum $kurikulum
     */
    public function __construct(Kurikulum $kurikulum)
    {
        $this->kurikulum = $kurikulum;
    }

    /**
     * @return Resource
     */
    public function getAll()
    {
        $kurikulumBuilder = $this->kurikulum->select();
        $kurikulumResource = $this->makeDynamicResource($kurikulumBuilder);

        return $kurikulumResource;
    }

    /**
     * @param $kurikulum_id
     * @return mixed
     */
    public function show($kurikulum_id)
    {
        $kurikulum = $this->kurikulum->findOrFail($kurikulum_id);

        return $kurikulum;
    }

    /**
     * @param $kurikulum_id
     * @return Resource
     */
    public function getMataKuliah($kurikulum_id)
    {
        $mataKuliahBuilder = $this->kurikulum->findOrFail($kurikulum_id)->mata_kuliah()->getQuery();
        $mataKuliahResource = $this->makeDynamicResource($mataKuliahBuilder);

        return $mataKuliahResource;
    }

    /**
     * @param $kurikulum_id
     * @param $mata_kuliah_id
     * @return mixed
     */
    public function showMataKuliahFromKurikulum($kurikulum_id, $mata_kuliah_id)
    {
        $mata_kuliah = $this->kurikulum->findOrFail($kurikulum_id)->mata_kuliah()->findOrFail($mata_kuliah_id);

        return $mata_kuliah;
    }

}