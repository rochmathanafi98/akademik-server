<?php

namespace FE_UNSIQ\Repositories\Prodi;

use FE_UNSIQ\Eloquent\SMS;
use FE_UNSIQ\Repositories\Repository;

class ProdiRepository extends Repository
{

    /**
     * @var SMS
     */
    protected $prodi;

    /**
     * ProdiRepository constructor.
     * @param SMS $prodi
     */
    public function __construct(SMS $prodi)
    {
        $this->prodi = $prodi;
    }

    /**
     * @return Resource
     */
    public function getAll()
    {
        $prodiBuilder = $this->prodi->select();
        $prodiResource = $this->makeDynamicResource($prodiBuilder);

        return $prodiResource;
    }

    /**
     * @param $prodi_id
     * @return Resource
     */
    public function getMataKuliah($prodi_id)
    {
        $mataKuliahBuilder = $this->prodi->findOrFail($prodi_id)->mata_kuliah()
            ->join('kelas_kuliah', 'mata_kuliah.id_mk', '=', 'kelas_kuliah.id_mk')
            ->getQuery();
        $mataKuliahResource = $this->makeDynamicResource($mataKuliahBuilder);

        return $mataKuliahResource;
    }

    /**
     * @param $prodi_id
     * @param $mata_kuliah_id
     * @return mixed
     */
    public function showMataKuliahFromProdi($prodi_id, $mata_kuliah_id)
    {
        $mataKuliahItem = $this->prodi
            ->findOrFail($prodi_id)
            ->mata_kuliah()
            ->findOrFail($mata_kuliah_id);

        return $mataKuliahItem;
    }

    /**
     * @param $prodi_id
     * @return Resource
     */
    public function getMahasiswaKeluar($prodi_id)
    {
        $mahasiswaKeluarBuilder = $this->prodi->findOrFail($prodi_id)
            ->mahasiswa()
            ->join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->leftJoin('jenis_keluar', 'jenis_keluar.id_jns_keluar', '=', 'mahasiswa_pt.id_jns_keluar')
            ->whereIn('stat_pd', ['D', 'K', 'L'])
            ->getQuery();

        $mahasiswaKeluarResource = $this->makeDynamicResource($mahasiswaKeluarBuilder);

        return $mahasiswaKeluarResource;
    }

    /**
     * @param $prodi_id
     * @param $mahasiswa_keluar_id
     * @return mixed
     */
    public function showMahasiswaKeluarFromProdi($prodi_id, $mahasiswa_keluar_id)
    {
        $mahasiswaKeluarItem = $this->prodi->findOrFail($prodi_id)
            ->mahasiswa()
            ->join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->whereIn('stat_pd', ['D', 'K', 'L'])
            ->getQuery()
            ->findOrFail($mahasiswa_keluar_id);

        return $mahasiswaKeluarItem;
    }
}