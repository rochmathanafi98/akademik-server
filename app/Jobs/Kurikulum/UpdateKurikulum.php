<?php

namespace FE_UNSIQ\Jobs\Kurikulum;

use FE_UNSIQ\Eloquent\Kurikulum;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateKurikulum extends Job implements SelfHandling
{
    protected $kurikulum;
    protected $requestData;

    public function __construct($kurikulum_id, $requestData)
    {
        $this->kurikulum = Kurikulum::findOrFail($kurikulum_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        foreach (\Schema::getColumnListing($this->kurikulum->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $this->kurikulum->$fieldName = $this->requestData[$fieldName];
            }
        }

        if ($this->kurikulum->save()) {
            return $this->kurikulum;
        }

        return false;
    }
}