<?php

namespace FE_UNSIQ\Jobs\Kurikulum;

use FE_UNSIQ\Eloquent\Kurikulum;
use FE_UNSIQ\Jobs\Job;
use FE_UNSIQ\Services\Feeder\WSClient;
use Illuminate\Contracts\Bus\SelfHandling;
use Webpatser\Uuid\Uuid;

class CreateSyncKurikulum extends Job implements SelfHandling
{
    protected $requestData;
    protected $feederClient;

    public function __construct($requestData)
    {
        $this->requestData = $requestData;
        $this->feederClient = new WSClient;
    }

    public function handle()
    {
        $dataKurikulum = $this->requestData;

        $dataKurikulum['id_kurikulum_sp'] = Uuid::generate(4);

        if ($this->feederClient->syncStatus) {
            $resource = $this->feederClient->insertRecord('kurikulum', $dataKurikulum);
            $dataKurikulum['id_kurikulum_sp'] = $resource->result->id_kurikulum_sp;
        }

        $kurikulum = new Kurikulum($dataKurikulum);
        if ($kurikulum->save()) {
            return $kurikulum;
        }
        return false;

    }
}