<?php

namespace FE_UNSIQ\Jobs\Kurikulum;

use FE_UNSIQ\Eloquent\MataKuliahKurikulum;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class DestroyMataKuliahFromKurikulum extends Job implements SelfHandling
{
    protected $makulKurikulum;

    public function __construct($kurikulum_id, $mata_kuliah_id)
    {
        $this->makulKurikulum = MataKuliahKurikulum::findOrFail($mata_kuliah_id);
    }

    public function handle()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        if ($this->makulKurikulum->delete()) {
            return $this->makulKurikulum;
        }else{
            return false;
        }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}