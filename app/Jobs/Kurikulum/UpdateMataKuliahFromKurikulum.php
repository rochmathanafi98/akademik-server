<?php

namespace FE_UNSIQ\Jobs\Kurikulum;

use FE_UNSIQ\Eloquent\MataKuliah;
use FE_UNSIQ\Eloquent\MataKuliahKurikulum;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateMataKuliahFromKurikulum extends Job implements SelfHandling
{

    protected $mataKuliah;
    protected $requestData;

    public function __construct($id_mata_kuliah_kurikulum, $requestData)
    {
        $this->mataKuliahKurikulum = MataKuliahKurikulum::findOrFail($id_mata_kuliah_kurikulum);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        foreach (\Schema::getColumnListing($this->mataKuliahKurikulum->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $this->mataKuliahKurikulum->$fieldName = $this->requestData[$fieldName];
            }
        }

        if ($this->mataKuliahKurikulum->save() ){
            return $this->mataKuliahKurikulum;
        }
        return false;
    }
}