<?php

namespace FE_UNSIQ\Jobs\Kurikulum;

use FE_UNSIQ\Eloquent\MataKuliahKurikulum;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateMataKuliahFromKurikulum extends Job implements SelfHandling
{
    protected $requestData;

    public function __construct($kurikulum_id, array $requestData)
    {
        $this->requestData = $requestData;
    }

    public function handle()
    {
        $mkkResult = MataKuliahKurikulum::create($this->requestData);
        return $mkkResult;
    }
}