<?php

namespace FE_UNSIQ\Jobs\MataKuliah;

use FE_UNSIQ\Eloquent\MataKuliah;
use FE_UNSIQ\Jobs\Job;
use FE_UNSIQ\Services\Feeder\WSClient;
use Illuminate\Contracts\Bus\SelfHandling;
use Webpatser\Uuid\Uuid;

class CreateSyncMataKuliah extends Job implements SelfHandling
{
    protected $requestData;
    protected $feederClient;

    public function __construct($requestData)
    {
        $this->requestData = $requestData;
        $this->feederClient = new WSClient;
    }

    public function handle()
    {
        $dataMataKuliah = $this->requestData;
        $dataMataKuliah['id_mk'] = Uuid::generate(4);

        if ($this->feederClient->syncStatus) {
            $resource = $this->feederClient->insertRecord('mata_kuliah', $dataMataKuliah);
            $dataMataKuliah['id_mk'] = $resource->result->id_mk;
        }

        $mataKuliah = new MataKuliah($dataMataKuliah);

        if ($mataKuliah->save()) {
            return $mataKuliah;
        }

        return false;
    }
}