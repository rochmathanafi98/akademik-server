<?php

namespace FE_UNSIQ\Jobs\MataKuliah;

use FE_UNSIQ\Eloquent\MataKuliah;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateMataKuliah extends Job implements SelfHandling
{

    protected $mataKuliah;
    protected $requestData;

    public function __construct($mata_kuliah_id, $requestData)
    {
        $this->mataKuliah = MataKuliah::findOrFail($mata_kuliah_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        foreach (\Schema::getColumnListing($this->mataKuliah->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $this->mataKuliah->$fieldName = $this->requestData[$fieldName];
            }
        }

        return $this->mataKuliah->save();
    }
}