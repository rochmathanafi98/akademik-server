<?php

namespace FE_UNSIQ\Jobs\Mahasiswa;

use Illuminate\Contracts\Bus\SelfHandling;

class DestroyMahasiswaKeluarFromProdi extends UpdateMahasiswaKeluarFromProdi implements SelfHandling
{

    public function handle()
    {
        $this->emptyFields();

        return $this->mahasiswa->save();
    }

    protected function emptyFields()
    {
        $this->mahasiswa->id_jns_keluar = NULL;
        $this->mahasiswa->tgl_keluar = NULL;
        $this->mahasiswa->ket = NULL;
        $this->mahasiswa->jalur_skripsi = NULL;
        $this->mahasiswa->judul_skripsi = NULL;
        $this->mahasiswa->bln_awal_bimbingan = NULL;
        $this->mahasiswa->bln_akhir_bimbingan = NULL;
        $this->mahasiswa->sk_yudisium = NULL;
        $this->mahasiswa->tgl_sk_yudisium = NULL;
        $this->mahasiswa->ipk = NULL;
        $this->mahasiswa->no_seri_ijazah = NULL;
    }

}