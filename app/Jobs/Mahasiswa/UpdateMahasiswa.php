<?php

namespace FE_UNSIQ\Jobs\Mahasiswa;

use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Schema;

class UpdateMahasiswa extends Job implements SelfHandling
{
    protected $requestData;
    protected $mahasiswa;

    public function __construct($mahasiswa_id, array $requestData)
    {
        $this->mahasiswa = Mahasiswa::findOrFail($mahasiswa_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        $mahasiswaProfil = $this->mahasiswa->mahasiswa_profil;

        foreach (Schema::getColumnListing($mahasiswaProfil->getTable()) as $filedName) {
            if (isset($this->requestData[$filedName])) {
                $mahasiswaProfil->$filedName = $this->requestData[$filedName];
            }
        }
        return $mahasiswaProfil->save();
    }
}