<?php

namespace FE_UNSIQ\Jobs\Mahasiswa;

use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\SMS;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateMahasiswaKeluarFromProdi extends Job implements SelfHandling
{

    protected $prodi;
    protected $mahasiswa;
    protected $requestData;

    public function __construct($prodi_id, $mahasiswa_id, $requestData)
    {
        $this->prodi = SMS::findOrFail($prodi_id);
        $this->mahasiswa = Mahasiswa::join('mahasiswa', 'mahasiswa.id_pd', '=', 'mahasiswa_pt.id_pd')
            ->whereIn('stat_pd', ['D', 'L', 'K'])
            ->findOrFail($mahasiswa_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        $this->updateField('id_jns_keluar');
        $this->updateField('tgl_keluar');
        $this->updateField('ket');
        $this->updateField('jalur_skripsi');
        $this->updateField('judul_skripsi');
        $this->updateField('bln_awal_bimbingan');
        $this->updateField('bln_akhir_bimbingan');
        $this->updateField('sk_yudisium');
        $this->updateField('tgl_sk_yudisium');
        $this->updateField('ipk');
        $this->updateField('no_seri_ijazah');

        return $this->mahasiswa->save();
    }

    protected function updateField($fieldName)
    {
        if (isset($this->requestData[$fieldName])) {
            $this->mahasiswa->$fieldName = $this->requestData[$fieldName];
        }
    }
}