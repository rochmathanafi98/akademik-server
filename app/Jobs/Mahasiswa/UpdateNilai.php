<?php

namespace FE_UNSIQ\Jobs\Mahasiswa;

use FE_UNSIQ\Eloquent\Nilai;
use FE_UNSIQ\Jobs\Job;
use FE_UNSIQ\Services\Nilai\BobotNilaiConverter;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateNilai extends Job implements SelfHandling
{
    protected $requestData;
    protected $nilai_id;

    public function __construct($nilai_id, $requestData)
    {
        $this->requestData = $requestData;
        $this->nilai_id = $nilai_id;
    }

    public function handle()
    {

        $nilai = Nilai::findOrFail($this->nilai_id);

        foreach (\Schema::getColumnListing('nilai') as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $nilai->$fieldName = $this->requestData[$fieldName];

                if($this->requestData['nilai_angka'] == ''){
                    $nilai->nilai_angka = null;
                }

                //jika yang di isi combo nilai huruf aja
                if (isset($this->requestData['has_changes'])
                    && $this->requestData['has_changes'] == 'nilai_indeks') {

                    $nilai->nilai_angka = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->indeksToAngka($this->requestData['nilai_indeks']);

                    $nilai->nilai_huruf = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->indeksToHuruf($this->requestData['nilai_indeks']);
                } else { // yang di isi nilai angka saja
                    $nilai->nilai_indeks = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->angkaToIndeks($this->requestData['nilai_angka']);

                    $nilai->nilai_huruf = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->angkaToHuruf($this->requestData['nilai_angka']);
                }

            }
        }

        if ($nilai->save()) {
            return $nilai;
        }

        return false;
    }
}