<?php

namespace FE_UNSIQ\Jobs\KelasKuliah;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Eloquent\Semester;
use FE_UNSIQ\Jobs\Job;
use FE_UNSIQ\Services\Feeder\WSClient;
use Illuminate\Contracts\Bus\SelfHandling;
use Webpatser\Uuid\Uuid;

class CreateKelasKuliahFromSemester extends Job implements SelfHandling
{

    protected $semester;
    protected $requestData;
    protected $feederClient;

    public function __construct($semester_id, $requestData)
    {
        $this->feederClient = new WSClient();
        $this->semester = Semester::findOrFail($semester_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        $kelasData = [];
        $this->requestData['id_kls'] = Uuid::generate(4);

        if ($this->feederClient->syncStatus) {
            $dbFields = \Schema::getColumnListing('kelas_kuliah');
            $kelasData = array_intersect_key($this->requestData, array_flip($dbFields));
            $resource = $this->feederClient->insertRecord('kelas_kuliah', $kelasData);
            $this->requestData['id_kls'] = $resource->result->id_kls;
        }

        $kelasKuliah = new KelasKuliah();

        foreach (\Schema::getColumnListing($kelasKuliah->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $kelasKuliah->$fieldName = $this->requestData[$fieldName];
            }
        }

        $kelasKuliah->id_smt = $this->semester->id_smt;

        if ($kelasKuliah->save()) {
            return $kelasKuliah;
        }

        return false;
    }

}
