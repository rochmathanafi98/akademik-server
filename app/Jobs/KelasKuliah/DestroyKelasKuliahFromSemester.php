<?php

namespace FE_UNSIQ\Jobs\KelasKuliah;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class DestroyKelasKuliahFromSemester extends Job implements SelfHandling
{

    protected $kelasKuliah;

    public function __construct($kelas_kuliah_id)
    {
        $this->kelasKuliah = KelasKuliah::findOrFail($kelas_kuliah_id);
    }

    public function handle()
    {
        if ($this->kelasKuliah->delete() ) {
            return $this->kelasKuliah;
        }
        return false;
    }

}