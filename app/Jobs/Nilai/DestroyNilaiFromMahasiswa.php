<?php

namespace FE_UNSIQ\Jobs\Nilai;

use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class DestroyNilaiFromMahasiswa extends Job implements SelfHandling
{

    protected $mahasiswa;
    protected $nilai;

    public function __construct(Mahasiswa $mahasiswa, $nilai_id)
    {
        $this->mahasiswa = $mahasiswa;
        $this->nilai = $mahasiswa->nilai()->findOrFail($nilai_id);
    }

    public function handle()
    {
        if ($this->nilai->delete()){
            return $this->nilai;
        }
        return false;

    }
}