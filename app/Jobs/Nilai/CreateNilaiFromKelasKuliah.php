<?php

namespace FE_UNSIQ\Jobs\Nilai;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Eloquent\Nilai;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateNilaiFromKelasKuliah extends Job implements SelfHandling
{

    protected $nilai;
    protected $kelasKuliah;
    protected $requestData;

    public function __construct(KelasKuliah $kelasKuliah, $requestData)
    {
        $this->kelasKuliah = $kelasKuliah;
        $this->requestData = $requestData;
    }

    public function handle()
    {
        $nilai = new Nilai();

        foreach (\Schema::getColumnListing($nilai->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $nilai->$fieldName = $this->requestData[$fieldName];
            }
        }

        $nilai->kelas_kuliah()->associate($this->kelasKuliah);

        if ($nilai->save()){
            return $nilai;
        }
        return false;
    }
}