<?php

namespace FE_UNSIQ\Jobs\Nilai;

use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\Nilai;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateNilaiFromMahasiswa extends Job implements SelfHandling
{

    protected $nilai;
    protected $mahasiswa;
    protected $requestData;

    public function __construct(Mahasiswa $mahasiswa, $requestData)
    {
        $this->mahasiswa = $mahasiswa;
        $this->requestData = $requestData;
    }

    public function handle()
    {
        $nilai = new Nilai();

        foreach (\Schema::getColumnListing($nilai->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $nilai->$fieldName = $this->requestData[$fieldName];
            }
        }

        $nilai->mahasiswa()->associate($this->mahasiswa);

        if ($nilai->save()){
            return $nilai;
        }
        return false;
    }
}