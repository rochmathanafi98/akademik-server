<?php

namespace FE_UNSIQ\Jobs\Nilai;

use FE_UNSIQ\Eloquent\KelasKuliah;
use FE_UNSIQ\Eloquent\Nilai;
use FE_UNSIQ\Jobs\Job;
use FE_UNSIQ\Services\Nilai\BobotNilaiConverter;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateNilaiFromKelasKuliah extends Job implements SelfHandling
{

    protected $nilai;
    protected $requestData;

    public function __construct($kelas_kuliah_id, $nilai_id, $requestData)
    {
        $this->nilai = KelasKuliah::findOrFail($kelas_kuliah_id)->nilai()->findOrFail($nilai_id);
        $this->requestData = $requestData;
    }

    public function handle()
    {
        foreach (\Schema::getColumnListing($this->nilai->getTable()) as $fieldName) {
            if (isset($this->requestData[$fieldName])) {
                $this->nilai->$fieldName = $this->requestData[$fieldName];

                if($this->requestData['nilai_angka'] == ''){
                    $this->nilai->nilai_angka = null;
                }

                //jika yang di isi combo nilai huruf aja
                if (isset($this->requestData['has_changes'])
                    && $this->requestData['has_changes'] == 'nilai_indeks') {

                    $this->nilai->nilai_angka = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->indeksToAngka($this->requestData['nilai_indeks']);

                    $this->nilai->nilai_huruf = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->indeksToHuruf($this->requestData['nilai_indeks']);
                } else { // yang di isi nilai angka saja
                    $this->nilai->nilai_indeks = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->angkaToIndeks($this->requestData['nilai_angka']);

                    $this->nilai->nilai_huruf = (new BobotNilaiConverter($this->requestData['id_sms']))
                        ->angkaToHuruf($this->requestData['nilai_angka']);
                }
            }
        }

        if ($this->nilai->save()) {
            return $this->nilai;
        }

        return false;
    }
}