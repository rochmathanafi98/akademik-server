<?php

namespace FE_UNSIQ\Jobs\Dosen;

use FE_UNSIQ\Eloquent\AjarDosen;
use FE_UNSIQ\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

class DeleteAjarDosen extends Job implements SelfHandling
{
    protected $ajar_dosen;

    public function __construct($id_ajar_dosen)
    {
        $this->ajar_dosen = AjarDosen::findOrFail($id_ajar_dosen);
    }

    public function handle()
    {
        if ($this->ajar_dosen->delete() ){
            return $this->ajar_dosen;
        }

        return false;
    }
}