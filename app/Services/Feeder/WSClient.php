<?php

namespace FE_UNSIQ\Services\Feeder;

use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Support\MessageBag;

class WSClient
{
    protected $feederFactory;
    protected $proxy;
    public $syncStatus;

    public function __construct()
    {
        $this->init();
        $this->syncStatus = config('feeder')['ws_sync'];
    }

    private function init()
    {
        if ($this->syncStatus) {
            $this->feederFactory = new FeederFactory();
            $this->proxy = $this->feederFactory->getProxy();
        }
    }

    public function insertRecord($tableName, array $data)
    {
        $resource = $this->proxy->call('InsertRecord', [$this->feederFactory->generateToken(), $tableName, json_encode($data)]);

//        preg_match("(\'[\w\s]+\')", $resource->result->error_desc, $matches);

        if ($resource->error_code != '0') {
            throw new StoreResourceFailedException('Data Gagal disinkronkan', new MessageBag([$resource->error_code => $resource->error_desc]));
        } else if ($resource->result->error_code != '0') {
            throw new StoreResourceFailedException('Data Gagal disinkronkan', new MessageBag([$resource->result->error_code => $resource->result->error_desc]));
        }

        return $resource;
    }

    public function insertRecordset($tableName, array $data)
    {
        $resource = $this->proxy->call('InsertRecordSet', [$this->feederFactory->generateToken(), $tableName, json_encode($data)]);

        if ($resource->error_code != '0') {
            throw new FeederClientException($resource->error_desc);
        } else if ($resource->result->error_code != '0') {
            throw new FeederClientException($resource->result->error_desc);
        }

        return $resource;
    }

}