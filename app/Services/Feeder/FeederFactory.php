<?php

namespace FE_UNSIQ\Services\Feeder;

use Artisaninweb\SoapWrapper\Facades\SoapWrapper;

class FeederFactory
{
    protected $feederWS;
    protected $token;

    /**
     * FeederFactory constructor.
     */
    public function __construct()
    {
        // url web service milik unsiq ada di config
        $url = config('feeder')['ws_url'];

        // initalize SOAP Wrapper
//        $soapWrapper = new Wrapper();
        SoapWrapper::add(function ($service) use ($url) {
            $service->name('feederDikti');
            $service->wsdl($url);
        });

        // set the client wrapper
        SoapWrapper::service('feederDikti', function ($service) {
            $this->feederWS = $service;
        });

        // generate the token
//        $this->token = $this->generateToken();
    }

    /**
     * @return mixed|string
     * @throws FeederTokenException
     */
    public function generateToken()
    {
        return $this->getProxy()->call('GetToken', [
            'username' => config('feeder')['ws_username'],
            'password' => config('feeder')['ws_password']
        ]);

        // sessioned token feature!
        // uncomment this if you want to enable it!
//        if ( ! Session::has('feeder_dikti_token')) {
//            $expirationDate = $this->feederWS->call('getExpired', ['token' => Session::get('feeder_dikti_token')]);
//
//            if ($expirationDate->result == date('Y-m-d')) {
//                throw new FeederTokenException('Token sudah tidak berlaku');
//            }
//
//            $token = $this->feederWS->call('GetToken', [
//                'username' => config('feeder')['ws_username'],
//                'password' => config('feeder')['ws_password']
//            ]);
//
//            if ($token != 'ERROR: username/password salah') {
//                Session::put('feeder_dikti_token', $token);
//                $this->token = $token;
//            } else {
//                throw new FeederTokenException('username atau password feeder salah');
//            }
//
//        }
//
//        $this->token = Session::get('feeder_dikti_token');
//
//        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getProxy()
    {
        return $this->feederWS;
    }
}