<?php

namespace FE_UNSIQ\Services\Webix;

class Resource
{

    protected $builder;
    protected $items;

    /**
     * Resource constructor.
     * @param $builder
     */
    public function __construct($builder = NULL)
    {
        if ( ! is_null($builder)) {
            $this->builder = $builder;
            $this->items = $builder->get();
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection|static[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function count()
    {
        if ( ! is_null($this->builder)) {
            return $this->builder->count();
        }

        return count($this->items);
    }
}