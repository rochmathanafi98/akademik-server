<?php

namespace FE_UNSIQ\Services\Webix\Response;

use FE_UNSIQ\Services\Webix\Resource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;

trait ResponseHelper
{

    /**
     * @param Resource $resourceFromRepo
     * @param TransformerAbstract | callable $transformer
     * @param array $metaData
     * @return Response
     */
    public function responseWebixCollection(Resource $resourceFromRepo, $transformer = NULL, $metaData = [])
    {
        $manager = new Manager();

        $resource = $manager->createData(
            new Collection($resourceFromRepo->getItems(),
                is_null($transformer) ? function ($data) {
                    return $data->toArray();
                } : $transformer)
        );

        if (app()->make(Request::class)->has('start')) {
            $metaData['pos'] = intval(app()->make(Request::class)->get('start'));
        } else {
            $metaData['pos'] = 0;
        }

        $metaData['total_count'] = $resourceFromRepo->count();

        return response()->json(array_merge($resource->toArray(), $metaData));
    }

    public function responseWebixChart(Resource $resourceFromRepo, $transformer = NULL)
    {
        $manager = new Manager();

        $resource = $manager->createData(
            new Collection($resourceFromRepo->getItems(),
                is_null($transformer) ? function ($data) {
                    return $data->toArray();
                } : $transformer)
        );

        /*if (app()->make(Request::class)->has('start')) {
            $metaData['pos'] = intval(app()->make(Request::class)->get('start'));
        } else {
            $metaData['pos'] = 0;
        }

        $metaData['total_count'] = $resourceFromRepo->count();*/

        return response()->json($resource->toArray());
    }

    /**
     * @param Model $model
     * @param TransformerAbstract | callable $transformer
     * @return Response
     */
    public function responseWebixItem($model, $transformer = NULL)
    {
        $manager = new Manager();
        $item = new Item($model, is_null($transformer) ? function ($data) {
            return $data->toArray();
        } : $transformer);

        return response()->json($manager->createData($item)->toArray());
    }

    /**
     * @param $model
     * @param null $transformer
     * @param array $metaData
     * @return mixed
     */
    public function responseWebixCreate($model, $transformer = NULL, $metaData = [])
    {
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $resource = $manager->createData(
            new Item($model, is_null($transformer) ? function ($data) {
                return $data->toArray();
            } : $transformer)
        )->toArray();

        $metaData['message'] = 'Data berhasil ditambahkan';

        return response()->json(array_merge($resource, $metaData))->setStatusCode(201);
    }

    /**
     * @param $model
     * @param null $transformer
     * @param array $metaData
     * @return mixed
     */
    public function responseWebixUpdate($model, $transformer = NULL, $metaData = [])
    {
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $resource = $manager->createData(
            new Item($model, is_null($transformer) ? function ($data) {
                return $data->toArray();
            } : $transformer)
        )->toArray();

        $metaData['message'] = 'Perubahan berhasil disimpan';

        return response()->json(array_merge($resource, $metaData));
    }

    /**
     * @param $model
     * @param null $transformer
     * @param array $metaData
     * @return mixed
     */
    public function responseWebixDelete($model, $transformer = NULL, $metaData = [])
    {
        $manager = new Manager();

        $resource = $manager->createData(
            new Item($model, is_null($transformer) ? function ($data) {
                return $data->toArray();
            } : $transformer)
        )->toArray();

        $metaData['message'] = 'Data berhasil dihapus';

        return response()->json(array_merge($resource, $metaData))->setStatusCode(200);
    }

}