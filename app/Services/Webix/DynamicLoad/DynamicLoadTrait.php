<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad;

use FE_UNSIQ\Services\Webix\DynamicLoad\Decorators\DefaultLimiter;
use FE_UNSIQ\Services\Webix\DynamicLoad\Decorators\Filter;
use FE_UNSIQ\Services\Webix\DynamicLoad\Decorators\Limiter;
use FE_UNSIQ\Services\Webix\DynamicLoad\Decorators\Skipper;
use FE_UNSIQ\Services\Webix\Resource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Trait DynamicLoadTrait
 * Can be use for generating dynamic loading for webix datatable request
 * Just inject this trait on your repository or somewhere on your code :)
 * This trait need Illuminate\Http\Request $request property on the class that use this trait to be works!
 *
 * @package FE_UNSIQ\Services\Webix\DynamicLoad
 */
trait DynamicLoadTrait
{

    /**
     * Here the magic begin!
     *
     * @param Builder $builder
     * @return Resource
     */
    protected function makeDynamicResource(Builder $builder)
    {
        $request = app()->make(Request::class);

        $dynamicLoader = new DynamicLoader($request);
        $dynamicLoader->setQueryBuilder($builder);

        // dynamic loading
        if ($request->has('continue')) {
            $dynamicLoader = new DefaultLimiter($dynamicLoader);
        }

        // skip some data
        if ($request->has('start')) {
            $dynamicLoader = new Skipper($dynamicLoader);
        }

        // limit the data load
        if ($request->has('count')) {
            $dynamicLoader = new Limiter($dynamicLoader);
        }

        // filter the data
        if ($request->has('filter')) {
            $dynamicLoader = new Filter($dynamicLoader);
        }

        $builder = $dynamicLoader->generateQueryBuilder();
        $resource = new Resource($builder);

        return $resource;
    }
}