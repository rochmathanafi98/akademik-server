<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface DynamicLoadInterface
{
    /**
     * Generate builder from parsing some request data
     *
     * @return Builder
     */
    public function generateQueryBuilder();
}