<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad;

use FE_UNSIQ\Services\Webix\DynamicLoad\Contracts\DynamicLoadInterface;
use Illuminate\Http\Request;

/**
 * Abstract Class DynamicLoadDecorator
 * The decorator class you can implement for additional functionability of the dynamic loader service
 *
 * @package FE_UNSIQ\Services\Webix\DynamicLoad
 */
abstract class DynamicLoadDecorator implements DynamicLoadInterface
{
    
    /**
     * @var DynamicLoadInterface
     */
    protected $dynamicLoader;

    /**
     * @var Request
     */
    protected $request;

    /**
     * DynamicLoadDecorator constructor.
     * 
     * @param DynamicLoadInterface $dynamicLoader
     */
    public function __construct(DynamicLoadInterface $dynamicLoader)
    {
        $this->dynamicLoader = $dynamicLoader;
        $this->request = $dynamicLoader->getRequest();
    }

    /**
     * @return mixed
     */
    abstract public function generateQueryBuilder();

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}