<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad\Decorators;

use FE_UNSIQ\Services\Webix\DynamicLoad\Contracts\DynamicLoadInterface;
use FE_UNSIQ\Services\Webix\DynamicLoad\DynamicLoadDecorator;
use Illuminate\Database\Eloquent\Builder;
use PHPSQLParser\PHPSQLParser;
use Schema;

class Filter extends DynamicLoadDecorator
{

    /**
     * @var Builder
     */
    protected $queryBuilder;

    /**
     * @var PHPSQLParser
     */
    protected $phpSqlParser;

    /**
     * Filter constructor.
     * 
     * @param DynamicLoadInterface $dynamicLoader
     */
    public function __construct(DynamicLoadInterface $dynamicLoader)
    {
        parent::__construct($dynamicLoader);
        $this->queryBuilder = $dynamicLoader->generateQueryBuilder();
        $this->phpSqlParser = new PHPSQLParser();
    }

    /**
     * Generate builder from parsing some request data
     *
     * @return Builder
     */
    public function generateQueryBuilder()
    {
        // get the filter requests
        $filters = $this->request->get('filter');

        // get the tables_used on query as array
        $tablesUsed = $this->phpSqlParser->parse($this->queryBuilder->toSql());

        $resultFilters = [];

        // iterate for each table used from query
        foreach ($tablesUsed['FROM'] as $table) {
            $tableName = trim($table['table'], '`');
            $columnUsed = Schema::getColumnListing($tableName);

            // iterate for each filter request
            foreach ($filters as $filterKey => $filterValue) {

                // check if column fount on the table
                if (in_array($filterKey, $columnUsed)) {
                    $resultFilters[$tableName][] = [$filterKey => $filterValue];
                }
            }
        }

        foreach ($resultFilters as $tableName => $filters) {
            foreach ($filters as $filter) {
                foreach ($filter as $filterKey => $filterValue) {
                    if ($filterValue != '')
                        $this->queryBuilder->where($tableName. '.' . $filterKey, 'LIKE', '%' . $filterValue . '%');
                }
            }
        }

        return $this->queryBuilder;
    }

}