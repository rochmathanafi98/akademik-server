<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad\Decorators;

use FE_UNSIQ\Services\Webix\DynamicLoad\Contracts\DynamicLoadInterface;
use FE_UNSIQ\Services\Webix\DynamicLoad\DynamicLoadDecorator;
use Illuminate\Database\Eloquent\Builder;

class Skipper extends DynamicLoadDecorator
{

    /**
     * @var Builder
     */
    protected $queryBuilder;

    /**
     * Skipper constructor.
     *
     * @param DynamicLoadInterface $dynamicLoader
     */
    public function __construct(DynamicLoadInterface $dynamicLoader)
    {
        parent::__construct($dynamicLoader);
        $this->queryBuilder = $dynamicLoader->generateQueryBuilder();
    }

    /**
     * Generate builder from parsing some request data
     *
     * @return mixed
     */
    public function generateQueryBuilder()
    {
        $skip = intval($this->request->get('start')) - 1;

        return $this->queryBuilder->skip($skip);
    }
}