<?php

namespace FE_UNSIQ\Services\Webix\DynamicLoad\Decorators;

use FE_UNSIQ\Services\Webix\DynamicLoad\Contracts\DynamicLoadInterface;
use FE_UNSIQ\Services\Webix\DynamicLoad\DynamicLoadDecorator;

class DefaultLimiter extends DynamicLoadDecorator
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $queryBuilder;
    protected $limit = 25;

    /**
     * DefaultLimiter constructor.
     *
     * @param DynamicLoadInterface $dynamicLoader
     */
    public function __construct(DynamicLoadInterface $dynamicLoader)
    {
        parent::__construct($dynamicLoader);
        $this->queryBuilder = $this->dynamicLoader->generateQueryBuilder();
    }

    /**
     * Generate builder from parsing some request data
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function generateQueryBuilder()
    {
        return $this->queryBuilder;
    }

}