<?php

namespace FE_UNSIQ\Services\Semester;

/**
 * Class SemesterTranslator
 * Digunakan untuk keperluan menghitung semester
 * dengan format tahunSmt (ex: 20151).
 *
 * @package FE_UNSIQ\Services
 */
class SemesterFactory
{

    /**
     * @var string
     */
    protected $tahunSmt;

    /**
     * SemesterTranslator constructor.
     * @param string $tahunSmt
     */
    public function __construct($tahunSmt = NULL)
    {
        $this->tahunSmt = $tahunSmt;
    }

    /**
     * @param $tahunSmt
     * @return $this
     */
    public function setTahunSmt($tahunSmt)
    {
        $this->tahunSmt = $tahunSmt;

        return $this;
    }

    /**
     * Generate tahun semester
     * semester ganjil september - februari ex: 20131
     * semester genap maret-agustus ex: 20132
     * @param null $tahun
     * @param null $bulan
     * @return string tahun semester ex: 20142
     */
    public function generateTahunSmtForNow($tahun = null, $bulan = null)
    {
        $date = [
            'tahun' => $tahun != null ? $tahun : date('Y'),
            'bulan' => $bulan != null ? $bulan : date('m'),
        ];

        if ($date['bulan'] >= 9 && $date['bulan'] <= 12)
            $thsms = $date['tahun'].'1';
        if ($date['bulan'] >= 1 && $date['bulan'] <= 2 )
            $thsms = ($date['tahun']-1).'1';
        if ($date['bulan'] >= 3 && $date['bulan'] <= 8 )
            $thsms = ($date['tahun']-1).'2';

        /** @var $thsms $thsms */
        return $thsms;
    }

    /**
     * Menghitung jumlah semester tempuh sampai waktu sekarang
     * @return float
     * @throws SemesterException
     */
    public function convertToSingle()
    {
        if (is_null($this->tahunSmt)) {
            throw new SemesterException('Tidak bisa menghitung semester dengan kondisi semester lama adalah NULL');
        }

        $tahunSmtNow = (int) $this->generateTahunSmtForNow(); //20152
        $semesterPast = (int) substr($this->tahunSmt, 0, 4); //20152

        if ($tahunSmtNow % 2 != 0) {
            $a = (($tahunSmtNow + 10) - 1) / 10;
            $b = $a - $semesterPast;
            $c = ($b * 2) - 1;

        } else {
            $a = (($tahunSmtNow + 10) - 2) / 10;
            $b = $a - $semesterPast;
            $c = $b * 2;

            //jika mulai_smt mahasiswa di semester genap
            if ($this->tahunSmt % 2 == 0 ) {
                $c = ($b * 2) - 1;
            }
        }

        return $c;
    }

}