<?php

namespace FE_UNSIQ\Services\Auth;

use Tymon\JWTAuth\Exceptions\JWTException;

class ApiAuthenticatorException extends JWTException
{
    //
}