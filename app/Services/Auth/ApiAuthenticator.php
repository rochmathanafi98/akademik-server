<?php

namespace FE_UNSIQ\Services\Auth;

use FE_UNSIQ\Eloquent\Administrator;
use FE_UNSIQ\Eloquent\Mahasiswa;
use FE_UNSIQ\Eloquent\User;
use Illuminate\Auth\Guard;
use Illuminate\Support\Collection;
use Tymon\JWTAuth\JWTAuth;

class ApiAuthenticator
{

    /**
     * @var Guard
     */
    protected $laravelAuth;

    /**
     * @var JWTAuth
     */
    protected $JWTAuth;

    /**
     * ApiAuthenticator constructor.
     *
     * @param JWTAuth $JWTAuth
     * @param Guard $laravelAuth
     */
    public function __construct(JWTAuth $JWTAuth, Guard $laravelAuth)
    {
        $this->laravelAuth = $laravelAuth;
        $this->JWTAuth = $JWTAuth;
    }

    /**
     * Authenticate user using their credentials
     *
     * @param array $credentials
     * @return string
     * @throws ApiAuthenticatorException
     */
    public function authenticate(array $credentials)
    {
        if (!$this->laravelAuth->attempt($credentials)) {
            throw new ApiAuthenticatorException('Invalid credentials', 403);
        }
        return $this->generateTokenFromUser();
    }

    /**
     * Generatiing JWT token from logged in user
     *
     * @return string
     * @throws ApiAuthenticatorException
     */
    public function generateTokenFromUser()
    {
        if (!$this->laravelAuth->check()) {
            throw new ApiAuthenticatorException('Could not generate token from an unauthenticated user', 401);
        }

        $user = $this->laravelAuth->user();
        $payloadData = $this->generatePayloadData($user);
        $token = $this->JWTAuth->fromUser($user, ['data' => $payloadData]);

        return $token;
    }

    /**
     * Generate payload data that will be store in the JTW token
     *
     * @param User $user
     * @return Collection
     */
    protected function generatePayloadData(User $user)
    {
        $data = new Collection([
            'username' => $user->username,
            'roles' => $user->listRoles(),
        ]);

        if ($user->owner_type == Mahasiswa::class) {
            $data->put('owner', [
                'type' => 'Mahasiswa',
                'name' => $user->owner->mahasiswa_profil->nm_pd,
                'id_reg_pd' => $user->owner->id_reg_pd,
                'id_sms' => $user->owner->sms->id_sms,
                'nama_prodi' => trim($user->owner->sms->nm_lemb),
                'semester_tempuh' => $user->owner->getSemesterTempuh(),
                'semester_aktif' => $user->owner->getSemesterTempuh(),
                'kelas' => $user->owner->nm_rombel,
            ]);
        } else if ($user->owner_type == Administrator::class) {
            $data->put('owner', [
                'type' => 'Administrator',
                'name' => $user->owner->name,
            ]);
        }

        return $data;
    }

}