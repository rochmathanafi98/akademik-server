<?php
/**
 * Created by IntelliJ IDEA.
 * User: aviq baihaqy
 * Date: 9/20/2016
 * Time: 12:23 AM
 */

namespace FE_UNSIQ\Services\Nilai;

use FE_UNSIQ\Eloquent\SMS;

class BobotNilaiConverter
{

    public function __construct($id_sms)
    {
        $this->sms = SMS::findOrFail($id_sms);
    }

    public function indeksToAngka($nilaiIndeks)
    {

        $nilaiAngka = $this->sms->bobot_nilai()
            ->where('nilai_indeks', '=', $nilaiIndeks)
            ->first();

        return $nilaiAngka->bobot_nilai_maks;
    }

    public function indeksToHuruf($nilaiIndeks)
    {
        $nilaiHuruf = $this->sms->bobot_nilai()
            ->where('nilai_indeks', '=', $nilaiIndeks)
            ->first();

        return $nilaiHuruf->nilai_huruf;
    }

    public function angkaToIndeks($nilaiAngka)
    {
        if($nilaiAngka == '') {
            return null;
        }

        $nilaiIndeks = $this->sms->bobot_nilai()
            ->where('bobot_nilai_min','<=', $nilaiAngka)
            ->where('bobot_nilai_maks','>=', $nilaiAngka)
            ->first();

        return $nilaiIndeks->nilai_indeks;
    }

    public function angkaToHuruf($nilaiAngka)
    {
        if($nilaiAngka == '') {
            return null;
        }

        $nilaiHuruf = $this->sms->bobot_nilai()
            ->where('bobot_nilai_min','<=', $nilaiAngka)
            ->where('bobot_nilai_maks','>=', $nilaiAngka)
            ->first();

        return $nilaiHuruf->nilai_huruf;
    }

}