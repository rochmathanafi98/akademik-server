<?php

namespace FE_UNSIQ\Services\Nilai;

class NilaiCalculator
{
    protected $allNilai;

    public function __construct($allNilai = [])
    {
        $this->allNilai = $allNilai->toArray();
    }

    protected function calculateIPK()
    {
        $calc = new \stdClass();
        $calc->total_sks = 0;
        $calc->total_nm = 0;
        $calc->ipk = 0;
        $nilai_mutu = 0;

        foreach ($this->allNilai as $nilai) {
            $nilai_mutu = $nilai['nilai_indeks'] * $nilai['sks_mk'];

            $calc->total_sks += $nilai['sks_mk'];
            $calc->total_nm += $nilai_mutu;
        }
        if ($calc->total_nm > 0)
            $calc->ipk = $calc->total_nm / $calc->total_sks;

        return $calc;
    }

    protected function getPredicate()
    {
        $calc = $this->calculateIPK();

        if ($calc->ipk > 3.5)
            $predicate = 'CUMLAUDE';
        else if ($calc->ipk > 3)
            $predicate = 'SANGAT MEMUASKAN';
        else if ($calc->ipk > 2.5)
            $predicate = 'MEMUASKAN';
        else if ($calc->ipk > 2)
            $predicate = 'CUKUP';
        else
            $predicate = '-';

        return $predicate;
    }

    public function getIPK()
    {
        $ipk = new \stdClass();
        $calc = $this->calculateIPK();

        $ipk->number = round($calc->ipk, 2);
        $ipk->total_sks = $calc->total_sks;
        $ipk->total_nm = $calc->total_nm;
        $ipk->predicate = $this->getPredicate();

        return $ipk;
    }
}